<?php
/**
 * Front-end in dit bestand is gemaakt door Melissa Vriend - MD3A
 * PHP in dit bestand is gemaakt door Maurice de Jong - MD3A
 */
//  Begin Maurice
require_once('includes/php/db_connection.php');


$page_id = "stage";
$getAllPageContent_sql = $db->prepare('SELECT `title`, `value` FROM `page_content` WHERE `page_id` = ?');
$getAllPageContent_sql->bindParam(1, $page_id);
$getAllPageContent_sql->execute();

while ($getAllPageContent_result = $getAllPageContent_sql->fetch(PDO::FETCH_OBJ)) {
    $pageContent[$getAllPageContent_result->title] = nl2br(htmlspecialchars(strip_tags($getAllPageContent_result->value), ENT_COMPAT,'ISO-8859-1', true));
//        $pageContent[$getAllPageContent_result->title] = nl2br(htmlspecialchars_decode($getAllPageContent_result->value));
}

//  Eind Maurice

?>
<!DOCTYPE html>
<html lang="nl" dir="ltr">

<head>
    <?php include_once("includes/metatags.php"); ?>
    <link rel="canonical" href="https://<?php echo htmlentities($_SERVER['SERVER_NAME']); ?>/stage" />
    <meta name="application-name" content="MaMedia">
    <title>MaMedia - Werk mee aan videoproducties</title>
    <meta name="description" content="Mamedia bied je een unieke stageplaats, waar je verantwoordelijkehid, ruimte om te experimenteren en veel creatieve vrijheid krijgt. Mamedia is het interne leer- en productiebedrijf van het Mediacollege Amsterdam.">
    <meta name="keywords" content="mamedia, mediacollege, amsterdam, stagiair, videoproductie, animaties, grafische vormgeving, webdesign">
    <script src='https://www.google.com/recaptcha/api.js' async defer></script>
</head>

<body>
    <?php include_once("includes/nav.php"); ?>

    <!-- begin Gradient  -->
    <div class="wrapper_gradient">
        <div class="mid-container">
            <div class="video_text">
                <!-- filmpje class  -->
                <div class="stage_video">
                    <iframe src="https://www.youtube-nocookie.com/embed/<?php echo $pageContent["stage_videoURL"]; ?>?loop=1&controls=0&showinfo=0&autoplay=1&mute=1&playlist=<?php echo $pageContent["stage_videoURL"]; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>

            <div class="video_text">
                <div class="stage_text">
                    <h1><?php echo $pageContent["stage_pageTitle"]; ?></h1>
                    <p>
                        <?php echo $pageContent["stage_pageDescription"]; ?>
                    </p>
                </div>
            </div>
            <div class="clear"></div>
        </div>


        <!-- Begin Quote fade in fade out -->

<!--        <div class="big_quote">-->
<!--            <h1><span><svg width="23.999999999999996" height="23.999999999999996" xmlns="http://www.w3.org/2000/svg">-->
<!---->
<!--                        <g>-->
<!--                            <title>background</title>-->
<!--                            <rect fill="none" id="canvas_background" height="402" width="582" y="-1" x="-1" />-->
<!--                        </g>-->
<!--                        <g>-->
<!--                            <title>Layer 1</title>-->
<!--                            <path fill="#ffffff" id="svg_1" d="m13.944902,21.036049l0,-7.391c0,-5.704 3.731,-9.57 8.983,-10.609l0.995,2.151c-2.432,0.917 -3.995,3.638 -3.995,5.849l4,0l0,10l-9.983,0zm-14.017,0l0,-7.391c0,-5.704 3.748,-9.57 9,-10.609l0.996,2.151c-2.433,0.917 -3.996,3.638 -3.996,5.849l3.983,0l0,10l-9.983,0z" />-->
<!--                        </g>-->
<!--                    </svg></span>-->
<!---->
<!--                <div id="testimonial-container"><span>Lorem nogwat ipsum</span>-->
<!--                    <span>ipsum nogwat lorum</span>-->
<!--                </div>-->
<!---->
<!--                <span><svg width="23.999999999999996" height="23.999999999999996" xmlns="http://www.w3.org/2000/svg">-->
<!---->
<!--                        <g>-->
<!--                            <title>background</title>-->
<!--                            <rect fill="none" id="canvas_background" height="402" width="582" y="-1" x="-1" />-->
<!--                        </g>-->
<!--                        <g>-->
<!--                            <title>Layer 1</title>-->
<!--                            <path transform="rotate(-180 11.927902221679688,12.036048889160156) " fill="#ffffff" id="svg_1" d="m13.944902,21.036049l0,-7.391c0,-5.704 3.731,-9.57 8.983,-10.609l0.995,2.151c-2.432,0.917 -3.995,3.638 -3.995,5.849l4,0l0,10l-9.983,0zm-14.017,0l0,-7.391c0,-5.704 3.748,-9.57 9,-10.609l0.996,2.151c-2.433,0.917 -3.996,3.638 -3.996,5.849l3.983,0l0,10l-9.983,0z" />-->
<!--                        </g>-->
<!--                    </svg></span>-->
<!--            </h1>-->
<!--        </div>-->




        <div class="big_quote">
            <span><svg width="23.999999999999996" height="23.999999999999996" xmlns="http://www.w3.org/2000/svg">

                        <g>
                            <title>background</title>
                            <rect fill="none" id="canvas_background" height="402" width="582" y="-1" x="-1" />
                        </g>
                        <g>
                            <title>Layer 1</title>
                            <path fill="#ffffff" id="svg_1" d="m13.944902,21.036049l0,-7.391c0,-5.704 3.731,-9.57 8.983,-10.609l0.995,2.151c-2.432,0.917 -3.995,3.638 -3.995,5.849l4,0l0,10l-9.983,0zm-14.017,0l0,-7.391c0,-5.704 3.748,-9.57 9,-10.609l0.996,2.151c-2.433,0.917 -3.996,3.638 -3.996,5.849l3.983,0l0,10l-9.983,0z" />
                        </g>
                    </svg>

                <span id="testimonialValue"></span>

                <svg width="23.999999999999996" height="23.999999999999996" xmlns="http://www.w3.org/2000/svg">

                        <g>
                            <title>background</title>
                            <rect fill="none" id="canvas_background" height="402" width="582" y="-1" x="-1" />
                        </g>
                        <g>
                            <title>Layer 1</title>
                            <path transform="rotate(-180 11.927902221679688,12.036048889160156) " fill="#ffffff" id="svg_1" d="m13.944902,21.036049l0,-7.391c0,-5.704 3.731,-9.57 8.983,-10.609l0.995,2.151c-2.432,0.917 -3.995,3.638 -3.995,5.849l4,0l0,10l-9.983,0zm-14.017,0l0,-7.391c0,-5.704 3.748,-9.57 9,-10.609l0.996,2.151c-2.433,0.917 -3.996,3.638 -3.996,5.849l3.983,0l0,10l-9.983,0z" />
                        </g>
                    </svg>
            </span>
        </div>




        <!-- end quote fade in fade out -->

    </div>
    <!-- end gradient  -->

    <!-- begin Amsterdammertjes -->

    <div id="amsterdammertjesSection">

        <div class="mid-container">

            <div id="amsterdammertjesLeft">
                <img src="<?php echo $pageContent["stage_amsterdammertjes_image"]; ?>" alt="Amsterdammertjes">
            </div>
            <div id="amsterdammertjesRight">
                <h2><?php echo $pageContent["stage_amsterdammertjes_title"]; ?></h2>
                <p><?php echo $pageContent["stage_amsterdammertjes_description"]; ?></p>
                <div id="amsterdammertjesMediaHolder">
                  <a href="http://www.salto.nl/programma/amsterdammertjes/" target="_blank">
                      <button class="whiteButtonBottom"><?php echo $pageContent["stage_amsterdammertjes_button_text"]; ?></button>
                  </a>
                    <a href="<?php echo $pageContent["stage_amsterdammertjes_youtube_link"]; ?>" target="_blank" id="youtubeButton2"> <img src="images/youtubelogowhite.svg" alt="YouTube logo"> </a>
                    <a href="<?php echo $pageContent["stage_amsterdammertjes_instagram_link"]; ?>" target="_blank" id="instagramButton2"> <img src="images/instagram.svg" alt="Instagram icon"> </a>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>


    <!-- Begin Formulier stage -->

<!-- <div class="wrapper_form">
  <div class="mid-container"> -->
    <!-- begin frank dropdown -->
<div id="dropDownWrapper">

  <div class="dropDownContainer">
    <button id="dropDownButton1" class="dropDown"><?php echo $pageContent["stage_dropdown_title1"]; ?></button>
    <div id="dropDownContent1" class="dropDownContentContainer">
    <p><?php echo $pageContent["stage_dropdown_text1"]; ?></p>
    </div>
  </div>

  <div class="dropDownContainer">
    <button id="dropDownButton2" class="dropDown"><?php echo $pageContent["stage_dropdown_title2"]; ?></button>
    <div id="dropDownContent2" class="dropDownContentContainer">
    <p><?php echo $pageContent["stage_dropdown_text2"]; ?></p>
    </div>
  </div>

  <div class="dropDownContainer">
    <button id="dropDownButton3" class="dropDown"><?php echo $pageContent["stage_dropdown_title3"]; ?></button>
    <div id="dropDownContent3" class="dropDownContentContainer">
    <p><?php echo $pageContent["stage_dropdown_text3"]; ?></p>
  </div>
    </div>

    <div class="dropDownContainer">
      <button id="dropDownButton4" class="dropDown"><?php echo $pageContent["stage_dropdown_title4"]; ?></button>
      <div id="dropDownContent4" class="dropDownContentContainer">
      <p><?php echo $pageContent["stage_dropdown_text4"]; ?></p>
      </div>
    </div>
    </div>
      <!-- end frank dropdown  -->
    <!-- </div>
</div> -->

    <div class="wrapper_form">
        <div class="mid-container">
            <div class="two_blocks">
                <div class="form_leftInfo">
                    <h1><?php echo $pageContent["stage_left_form_heading"]; ?></h1>
                            <p><?php echo $pageContent["stage_left_form_description"]; ?></p>

                </div>
            </div>
            <div class="two_blocks text-align-left">
                <div class="form_rightInfo">
                    <h1 class="text-align-center"><?php echo $pageContent["stage_form_title"]; ?></h1>
                    <div class="form_input">
                        <?php

                        if (isset($_GET["form"]) && !empty($_GET["form"]) && urldecode($_GET["form"]) == "Mail succesvol verzonden") {

                            echo '<div style="text-align: center;font-weight: 600;font-size: 21px;">'.htmlentities($_GET["form"]).'</div>';

                        } else {
                            $email = $functie = $motivatie = $form = "";
                            if (isset($_GET["email"])){
                                $email = $_GET["email"];
                            }
                            if (isset($_GET["functie"])){
                                $functie = $_GET["functie"];
                            }
                            if (isset($_GET["motivatie"])){
                                $motivatie = $_GET["motivatie"];
                            }
                            if (isset($_GET["form"])){
                                $form = $_GET["form"];
                            }
                            ?>
                            <div style="text-align: center;font-weight: 600;font-size: 21px;margin-bottom: 15px;"><?php echo htmlentities($form); ?></div>
                        <form id="stageFormulier" action="/php/stage_form_send.php" method="post" enctype="multipart/form-data">
                        <input type="text" name="email" placeholder="E-mail" id="form_email" value="<?php echo htmlentities($email); ?>" required>
                        <input type="text" name="functie" placeholder="Functie" id="form_functie" value="<?php echo htmlentities($functie); ?>" required>
                        <br>
                        <textarea rows="10" name="motivatie" placeholder="Motivatie" id="form_motivatie" required><?php echo htmlentities($motivatie); ?></textarea><br>
                        <div class="clear"></div>
                        <label for="form_attachment">Upload CV (.pdf)</label>
                        <input type="file" name="attachment"
                               accept="application/pdf,application/vnd.ms-excel" id="form_attachment" required>
                            <div class="g-recaptcha" data-sitekey="6Lc2_qIUAAAAAOliNxPzH3x6XjRaPQUg2O0cvhkx"></div>
                        <br>
                            <button name="submit" class="button1 send" id="stage_submit">Verstuur</button>
                        </form>
                        <?php } ?>

                    </div>
                </div>

            </div>
        </div>

    </div>






    <!-- melissa amsterdammertjes -->
    <!-- <div class="wrapper_Adam">
        <div class="flexing">
            <div class="Adam_logo">
                <img src="images/amsterdammertjes.png">
            </div>
            <div class="tekstje">
                <h1>Amsterdammertjes</h1>
                <p>Amsterdammertjes is ons live tv-programma op Salto1!
                    Het is een talkshow voor en door jongeren, met vaste items en een studiogast.
                    Bij Amsterdammertjes kan je je creatieve vrijheid kwijt. Je bedenkt de content, regelt
                    alles, filmt, edit, presenteert en je doet zelfs de techniek van de uitzending. Benieuwd gewoden?
                    Klik dan hier om de uitzendingen van Amsterdammertjes te kijken!
                </p>
                <div class="buttons_Adam">
                    <a href="#" class="button1">Bekijk&nbsp;nu</a>
                    <div class="social_wrapper">
                        <a href="#" class="buttonyt"><img class="socials" src="images/youtubelogowhite.svg"></a>
                        <a href="#" class="buttonig"><img class="socials" src="images/instagram.svg"></a>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- end Amsterdammertjes -->




    <!-- end formulier page -->



    <?php include_once("includes/footer.php");


    //  Begin Maurice
    $testimonials = array();
    $pagina = "stage";

    $getAllTestimonialItems_sql = $db->prepare('SELECT `testimonial_value` FROM testimonials WHERE `pagina` = ?');
    $getAllTestimonialItems_sql->bindParam(1, $pagina);
    $getAllTestimonialItems_sql->execute();

    while($getAllTestimonialItems_result = $getAllTestimonialItems_sql->fetch( PDO::FETCH_OBJ )) {
        array_push($testimonials, htmlentities($getAllTestimonialItems_result->testimonial_value));
    }

    $testimonial_items = array(
        "testimonials" => $testimonials,
    );
    //  Eind Maurice
    ?>



    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script>
        var testimonialResults = JSON.parse('<?php echo json_encode($testimonial_items); ?>');
    </script>
    <script src="js/quotefade.js"></script>
    <script src="/js/hamburgerMenu.js"></script>
    <!-- frank dropdown script start -->
  <script>
  $( document ).ready(function() {
  $("#dropDownContent1").toggle();
  $("#dropDownContent2").toggle();
  $("#dropDownContent3").toggle();
  $("#dropDownContent4").toggle();
  });

  $(document).ready(function(){
    $("#dropDownButton1").click(function(){
      $("#dropDownContent1").toggle();
    });
  });

  $(document).ready(function(){
    $("#dropDownButton2").click(function(){
      $("#dropDownContent2").toggle();
    });
  });

  $(document).ready(function(){
    $("#dropDownButton3").click(function(){
      $("#dropDownContent3").toggle();
    });
  });

  $(document).ready(function(){
    $("#dropDownButton4").click(function(){
      $("#dropDownContent4").toggle();
    });
  });

  </script>
  <!-- frank dropdown script end -->

</body>

</html>
