<?php
require_once('../../includes/php/db_connection.php');
$page_id = "samenwerken";
$getAllPageContent_sql = $db->prepare('SELECT `title`, `value` FROM `page_content` WHERE `page_id` = ?');
$getAllPageContent_sql->bindParam(1, $page_id);
$getAllPageContent_sql->execute();

while ($getAllPageContent_result = $getAllPageContent_sql->fetch(PDO::FETCH_OBJ)) {
    $pageContent[$getAllPageContent_result->title] = htmlspecialchars($getAllPageContent_result->value, ENT_COMPAT,'ISO-8859-1', true);
}

?>
<!DOCTYPE html>
<html lang="nl" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">
</head>
<body>


<div class="footerEditSection">
    <p class="thickFont">Testimonial overzicht.</p>
    <table>
        <tr>
            <th>Testimonial</th>
            <th>Actie</th>
        </tr>
        <?php
        $db_pagina = "samenwerken";
        $getAllTestimonials_sql = $db->prepare('SELECT `id`, `testimonial_value` FROM `testimonials` WHERE `pagina`=?');
        $getAllTestimonials_sql->bindParam(1, $db_pagina);
        $getAllTestimonials_sql->execute();
        while ($getAllTestimonials_result = $getAllTestimonials_sql->fetch(PDO::FETCH_OBJ)) {
            echo '<tr><td>' . htmlspecialchars($getAllTestimonials_result->testimonial_value, ENT_COMPAT,'ISO-8859-1', true) . '</td><td class="color-red"><a href="/admin/pages/saves/delete_testimonial.php?id=' . urlencode($getAllTestimonials_result->id) . '&pageRedirect=paginas-samenwerken">Verwijderen</a></td></tr>';
        }

        ?>

    </table>
</div>

<form action="/admin/pages/saves/add_testimonial.php" method="post">
    <div class="footerEditSection">
        <p class="thickFont">Testimonial toevoegen.</p>
        <input type="text" name="testimonial" placeholder="Testimonial" required><br>
        <input type="submit" class="submitButton" name="submit" value="Toevoegen">
        <input type="hidden" name="pageRedirect" value="paginas-samenwerken">
        <input type="hidden" name="pagina" value="samenwerken">
    </div>
</form>


<form action="/admin/pages/saves/update_page_content.php" method="post">

    <div class="footerEditSection">
        <p class="thickFont">Verander de inleiding van de pagina.</p>
        <p class="descriptionText">Inleiding kopje</p>
        <input type="text" name="samenwerken_pageTitle" value="<?php echo $pageContent["samenwerken_pageTitle"]; ?>"
               placeholder="Inleiding kopje"><br>
        <p class="descriptionText">Inleiding tekst</p>
        <textarea rows="8" cols="70%" name="samenwerken_pageDescription"
                  placeholder="Inleiding tekst"><?php echo $pageContent["samenwerken_pageDescription"]; ?></textarea><br>
        <p class="descriptionText">Samenwerken trailer video link</p>
        <input type="text" name="samenwerken_videoURL" value="https://www.youtube.com/watch?v=<?php echo $pageContent["samenwerken_videoURL"]; ?>"
               placeholder="Samenwerken trailer video link"><br>
    </div>

    <div class="footerEditSection">
        <p class="thickFont">Verander de tekst naast het contactform</p>
        <p class="descriptionText">Inleiding kopje</p>
        <input type="text" name="samenwerken_left_form_heading"
               value="<?php echo $pageContent["samenwerken_left_form_heading"]; ?>" placeholder="Samenwerken kopje"><br>
        <p class="descriptionText">Tekst naast het contactform</p>
        <textarea rows="8" cols="70%" name="samenwerken_left_form_description"
                  placeholder="Samenwerken tekst"><?php echo $pageContent["samenwerken_left_form_description"]; ?></textarea><br>
        <p class="descriptionText">Contact form titel</p>
        <input type="text" name="samenwerken_form_title" value="<?php echo $pageContent["samenwerken_form_title"]; ?>"
               placeholder="Contact form titel"><br>

    </div>

    <div class="footerEditSection">
        <p class="thickFont">Alles klaar?</p>
        <input type="submit" class="submitButton" name="submit" value="update">
        <input type="hidden" name="pageRedirect" value="paginas-samenwerken">
    </div>


</form>
</body>
</html>
