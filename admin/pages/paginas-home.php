<?php
require_once('../../includes/php/db_connection.php');
$page_id = "home";
$getAllPageContent_sql = $db->prepare('SELECT `title`, `value` FROM `page_content` WHERE `page_id` = ?');
$getAllPageContent_sql->bindParam(1, $page_id);
$getAllPageContent_sql->execute();

while ($getAllPageContent_result = $getAllPageContent_sql->fetch(PDO::FETCH_OBJ)) {
    $pageContent[$getAllPageContent_result->title] = htmlspecialchars($getAllPageContent_result->value, ENT_COMPAT,'ISO-8859-1', true);
}

?>
<!DOCTYPE html>
<html lang="nl" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">
</head>
<body>
<form action="/admin/pages/saves/update_page_content.php" method="post" enctype="multipart/form-data">
    <div class="footerEditSection">
        <p class="thickFont">Verander de media links</p>
        <p class="descriptionText">YouTube trailer video link</p>
        <input type="text" name="home_videoOverlay_videoURL"
               value="https://www.youtube.com/watch?v=<?php echo $pageContent["home_videoOverlay_videoURL"]; ?>" placeholder="YouTube video link"><br>
        <p class="descriptionText">YouTube social media button link</p>
        <input type="text" name="home_videoOverlay_socials_youtube_link"
               value="<?php echo $pageContent["home_videoOverlay_socials_youtube_link"]; ?>"
               placeholder="YouTube social media button link"><br>
        <p class="descriptionText">Facebook social media button link</p>
        <input type="text" name="home_videoOverlay_socials_facebook_link"
               value="<?php echo $pageContent["home_videoOverlay_socials_facebook_link"]; ?>"
               placeholder="Facebook social media button link"><br>
        <p class="descriptionText">Instagram social media button link</p>
        <input type="text" name="home_videoOverlay_socials_instagram_link"
               value="<?php echo $pageContent["home_videoOverlay_socials_instagram_link"]; ?>"
               placeholder="Instagram social media button link"><br>
    </div>

    <div class="footerEditSection">
        <p class="thickFont">Verander de Amsterdammertjes sectie voor de landingspage.</p>
        <p class="descriptionText">Amsterdammertjes afbeelding</p>
        <input type="file" name="home_amsterdammertjes_image"><br>

        <p class="descriptionText">Amsterdammertjes titel</p>
        <input type="text" name="home_amsterdammertjes_title"
               value="<?php echo $pageContent["home_amsterdammertjes_title"]; ?>"
               placeholder="Amsterdammertjes titel"><br>
        <p class="descriptionText">Amsterdammertjes beschrijving</p>
        <textarea rows="8" cols="70%" name="home_amsterdammertjes_description"
                  placeholder="amsterdammertjes tekst"><?php echo $pageContent["home_amsterdammertjes_description"]; ?></textarea><br>
        <p class="descriptionText">Amsterdammertjes button tekst</p>
        <input type="text" name="home_amsterdammertjes_button_text"
               value="<?php echo $pageContent["home_amsterdammertjes_button_text"]; ?>"
               placeholder="Amsterdammertjes button tekst"><br>

        <p class="thickFont">Social media van de amsterdammertjes sectie.</p>
        <p class="descriptionText">Amsterdammertjes YouTube link</p>
        <input type="text" name="home_amsterdammertjes_youtube_link"
               value="<?php echo $pageContent["home_amsterdammertjes_youtube_link"]; ?>"
               placeholder="Amsterdammertjes youtube link"><br>
        <p class="descriptionText">Amsterdammertjes Instagram link</p>
        <input type="text" name="home_amsterdammertjes_instagram_link"
               value="<?php echo $pageContent["home_amsterdammertjes_instagram_link"]; ?>"
               placeholder="Amsterdammertjes instagram link"><br>
    </div>


    <div class="footerEditSection">
        <p class="thickFont">Het team sectie</p>
        <table>
            <tr>
                <th>Foto</th>
                <th>Naam</th>
                <th>Beschrijving</th>
                <th>Actie</th>
            </tr>
            <?php
            $getAllTeammates_sql = $db->prepare('SELECT `id`, `name`, `description`, `image` FROM `colleague`');
            $getAllTeammates_sql->execute();

            while ($getAllTeammates_result = $getAllTeammates_sql->fetch(PDO::FETCH_OBJ)) {
                echo '<tr><td style="background-image: url(' . htmlentities($getAllTeammates_result->image) . '); height: 300px; width: 300px; background-position: top center; background-size: cover;"></td><td><strong>' . htmlentities($getAllTeammates_result->name) . '</strong></td><td>'.nl2br(htmlentities($getAllTeammates_result->description)).'</td><td><span class="cursor-pointer" onclick="$changeTeamMate(' . htmlentities($getAllTeammates_result->id) . ')">Wijzigen</span><br><br><a href="/admin/pages/saves/delete_teammate.php?teammateid=' . urlencode($getAllTeammates_result->id) . '" class="color-red">Verwijderen</a></td></tr>';
            }

            ?>

        </table><br>
        <div class="button-pink cursor-pointer" onclick='$loadContentInView("paginas-insert-teammate.php")'>Team lid toevoegen</div>
    </div>

    <div class="footerEditSection">
        <p class="thickFont">Samenwerken sectie</p>
        <p class="descriptionText">Samenwerken call to action titel</p>
        <input type="text" name="home_callToAction_title" value="<?php echo $pageContent["home_callToAction_title"]; ?>"
               placeholder="Samenwerken call to action titel"><br>
        <p class="descriptionText">Wordt stagair button</p>
        <input type="text" name="home_callToAction_stagiair"
               value="<?php echo $pageContent["home_callToAction_stagiair"]; ?>" placeholder="Wordt stagair button"><br>
        <p class="descriptionText">Samenwerken button</p>
        <input type="text" name="home_callToAction_samenwerken"
               value="<?php echo $pageContent["home_callToAction_samenwerken"]; ?>"
               placeholder="Samenwerken button"><br>
    </div>

    <div class="footerEditSection">
        <p class="thickFont">Alles klaar?</p>
        <input type="submit" class="submitButton" name="submit" value="update">
        <input type="hidden" name="pageRedirect" value="paginas-home">
    </div>
</form>
</body>
</html>
