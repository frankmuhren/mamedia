<?php
require_once('../../includes/php/db_connection.php');
$page_id = "stage";
$getAllPageContent_sql = $db->prepare('SELECT `title`, `value` FROM `page_content` WHERE `page_id` = ?');
$getAllPageContent_sql->bindParam(1, $page_id);
$getAllPageContent_sql->execute();

while ($getAllPageContent_result = $getAllPageContent_sql->fetch(PDO::FETCH_OBJ)) {
    $pageContent[$getAllPageContent_result->title] = htmlspecialchars($getAllPageContent_result->value, ENT_COMPAT,'ISO-8859-1', true);
}

?>
<!DOCTYPE html>
<html lang="nl" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">
</head>
<body>

    <div class="footerEditSection">
        <p class="thickFont">Testimonial overzicht.</p>
        <table>
            <tr>
                <th>Testimonial</th>
                <th>Actie</th>
            </tr>
            <?php
            $db_pagina = "stage";
            $getAllTestimonials_sql = $db->prepare('SELECT `id`, `testimonial_value` FROM `testimonials` WHERE `pagina`=?');
            $getAllTestimonials_sql->bindParam(1, $db_pagina);
            $getAllTestimonials_sql->execute();
            while ($getAllTestimonials_result = $getAllTestimonials_sql->fetch(PDO::FETCH_OBJ)) {
                echo '<tr><td>' . nl2br(htmlspecialchars($getAllTestimonials_result->testimonial_value
                        , ENT_COMPAT,'ISO-8859-1', true)) . '</td><td class="color-red"><a href="/admin/pages/saves/delete_testimonial.php?id=' . urlencode($getAllTestimonials_result->id) . '&pageRedirect=paginas-stage">Verwijderen</a></td></tr>';
            }

            ?>

        </table>
    </div>

    <form action="/admin/pages/saves/add_testimonial.php" method="post">
        <div class="footerEditSection">
            <p class="thickFont">Testimonial toevoegen.</p>
            <input type="text" name="testimonial" placeholder="Testimonial" required><br>
            <input type="submit" class="submitButton" name="submit" value="Toevoegen">
            <input type="hidden" name="pageRedirect" value="paginas-stage">
            <input type="hidden" name="pagina" value="stage">
        </div>
    </form>


    <form action="/admin/pages/saves/update_page_content.php" method="post" enctype="multipart/form-data">
    <div class="footerEditSection">
        <p class="thickFont">Verander de inleiding van de pagina.</p>
        <p class="descriptionText">Inleiding kopje</p>
        <input type="text" name="stage_pageTitle" value="<?php echo $pageContent["stage_pageTitle"]; ?>"
               placeholder="inleiding kopje"><br>
        <p class="descriptionText">Inleiding tekst</p>
        <textarea rows="8" cols="70%"
                  name="stage_pageDescription"><?php echo $pageContent["stage_pageDescription"]; ?></textarea><br>
        <p class="descriptionText">Stage trailer video link</p>
        <input type="text" name="stage_videoURL"
               value="https://www.youtube.com/watch?v=<?php echo $pageContent["stage_videoURL"]; ?>"
               placeholder="stage trailer video link"><br>
    </div>

    <div class="footerEditSection">
        <p class="thickFont">Verander het amsterdammertjes sectie van de pagina.</p>
        <p class="descriptionText">Amsterdammertjes afbeelding</p>
        <input type="file" name="stage_amsterdammertjes_image"><br>
        <p class="descriptionText">Amsterdammertjes kopje</p>
        <input type="text" name="stage_amsterdammertjes_title"
               value="<?php echo $pageContent["stage_amsterdammertjes_title"]; ?>" placeholder="amsterdammertjes kopje"><br>
        <p class="descriptionText">Amsterdammertjes tekst</p>
        <textarea rows="8" cols="70%" name="stage_amsterdammertjes_description"
                  placeholder="amsterdammertjes tekst"><?php echo $pageContent["stage_amsterdammertjes_description"]; ?></textarea><br>
        <p class="descriptionText">YouTube link</p>
        <input type="text" name="stage_amsterdammertjes_youtube_link"
               value="<?php echo $pageContent["stage_amsterdammertjes_youtube_link"]; ?>"
               placeholder="YouTube link"><br>
        <p class="descriptionText">Instagram link</p>
        <input type="text" name="stage_amsterdammertjes_instagram_link"
               value="<?php echo $pageContent["stage_amsterdammertjes_instagram_link"]; ?>" placeholder="Instagram"><br>
    </div>

    <div class="footerEditSection">
        <p class="thickFont">Verander de dropdown buttons.</p>
        <p class="descriptionText">Dropdown button 1, titel en tekst</p>
        <input type="text" name="stage_dropdown_title1" value="<?php echo $pageContent["stage_dropdown_title1"]; ?>"
               placeholder="Dropdown title 1"><br>
        <textarea rows="8" cols="70%" name="stage_dropdown_text1"
                  placeholder="Dropdown text 1"><?php echo $pageContent["stage_dropdown_text1"]; ?></textarea><br>
        <p class="descriptionText">Dropdown button 2, titel en tekst</p>
        <input type="text" name="stage_dropdown_title2" value="<?php echo $pageContent["stage_dropdown_title2"]; ?>"
               placeholder="Dropdown title 2"><br>
        <textarea rows="8" cols="70%" name="stage_dropdown_text2"
                  placeholder="Dropdown text 2"><?php echo $pageContent["stage_dropdown_text2"]; ?></textarea><br>
        <p class="descriptionText">Dropdown button 3, titel en tekst</p>
        <input type="text" name="stage_dropdown_title3" value="<?php echo $pageContent["stage_dropdown_title3"]; ?>"
               placeholder="Dropdown title 3"><br>
        <textarea rows="8" cols="70%" name="stage_dropdown_text3"
                  placeholder="Dropdown text 3"><?php echo $pageContent["stage_dropdown_text3"]; ?></textarea><br>

                  <p class="descriptionText">Dropdown button 4, titel en tekst</p>
                  <input type="text" name="stage_dropdown_title4" value="<?php echo $pageContent["stage_dropdown_title4"]; ?>"
                         placeholder="Dropdown title 4"><br>
                  <textarea rows="8" cols="70%" name="stage_dropdown_text4"
                            placeholder="Dropdown text 4"><?php echo $pageContent["stage_dropdown_text4"]; ?></textarea><br>
    </div>

    <div class="footerEditSection">
        <p class="thickFont">Verander de call to action.</p>
        <p class="descriptionText">Call to action kopje</p>
        <input type="text" name="stage_left_form_heading" value="<?php echo $pageContent["stage_left_form_heading"]; ?>"
               placeholder="call to action kopje"><br>
        <p class="descriptionText">Call to action tekst</p>
        <textarea rows="8" cols="70%" name="stage_left_form_description"
                  placeholder="call to action description"><?php echo $pageContent["stage_left_form_description"]; ?></textarea><br>
        <p class="descriptionText">Form titel</p>
        <input type="text" name="stage_form_title" value="<?php echo $pageContent["stage_form_title"]; ?>"
               placeholder="Form titel"><br>
    </div>

    <div class="footerEditSection">
        <p class="thickFont">Alles klaar?</p>
        <input type="submit" class="submitButton" name="submit" value="update">
        <input type="hidden" name="pageRedirect" value="paginas-stage">
    </div>

</form>
</body>
</html>
