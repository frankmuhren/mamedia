<?php
//Code in dit bestand is gemaakt door Maurice de Jong - MD3A
require_once('../../includes/php/db_connection.php');
$getAllPageContent_sql = $db->prepare('SELECT * FROM `form_settings`');
$getAllPageContent_sql->execute();

while ($getAllPageContent_result = $getAllPageContent_sql->fetch(PDO::FETCH_OBJ)) {
    if ($getAllPageContent_result->page_id == "stage") {
        $pageContent["stage_mail"] = htmlspecialchars($getAllPageContent_result->sendMailTo, ENT_COMPAT,'ISO-8859-1', true);
        $pageContent["stage_cc"] = htmlspecialchars($getAllPageContent_result->cc, ENT_COMPAT,'ISO-8859-1', true);
    } else {
        $pageContent["samenwerken_mail"] = htmlspecialchars($getAllPageContent_result->sendMailTo, ENT_COMPAT,'ISO-8859-1', true);
        $pageContent["samenwerken_cc"] = htmlspecialchars($getAllPageContent_result->cc, ENT_COMPAT,'ISO-8859-1', true);
    }

}

?>
<!DOCTYPE html>
<html lang="nl" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">
</head>
<body>
<form action="/admin/pages/saves/update_email_settings.php" method="post">
    <div class="footerEditSection">
        <p class="thickFont">Formulier instellingen</p>
        <p class="descriptionText"><strong>Stage</strong> formulier e-mailadres</p>
        <input type="email" name="stage_email"
               value="<?php echo $pageContent["stage_mail"]; ?>" placeholder="Stage formulier e-mailadres"><br>
        <input type="email" name="stage_cc"
               value="<?php echo $pageContent["stage_cc"]; ?>" placeholder="Stage formulier CC"><br><br><br>
        <p class="descriptionText"><strong>Samenwerken</strong> formulier e-mailadres</p>
        <input type="email" name="samenwerken_email"
               value="<?php echo $pageContent["samenwerken_mail"]; ?>"
               placeholder="Samenwerken formulier e-mailadres"><br>
        <input type="email" name="samenwerken_cc"
               value="<?php echo $pageContent["samenwerken_cc"]; ?>"
               placeholder="Samenwerken formulier CC"><br>
    </div>


    <div class="footerEditSection">
        <p class="thickFont">Alles klaar?</p>
        <input type="submit" class="submitButton" name="submit" value="update">
        <input type="hidden" name="pageRedirect" value="paginas-home">
    </div>
</form>
</body>
</html>
