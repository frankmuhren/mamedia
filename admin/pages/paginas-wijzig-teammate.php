<?php
//Code in dit bestand is gemaakt door Maurice de Jong - MD3A


require_once('../../includes/php/db_connection.php');
$id = $_GET["id"];
$getAllPageContent_sql = $db->prepare('SELECT `id`, `name`, `description`, `image` FROM `colleague` WHERE `id`=?');
$getAllPageContent_sql->bindParam(1, $id);
$getAllPageContent_sql->execute();

$getAllPageContent_result = $getAllPageContent_sql->fetch(PDO::FETCH_OBJ);

$pageContent["id"] = htmlspecialchars($getAllPageContent_result->id, ENT_COMPAT,'ISO-8859-1', true);
$pageContent["name"] = htmlspecialchars($getAllPageContent_result->name, ENT_COMPAT,'ISO-8859-1', true);
$pageContent["description"] = htmlspecialchars($getAllPageContent_result->description, ENT_COMPAT,'ISO-8859-1', true);
$pageContent["image"] = htmlspecialchars($getAllPageContent_result->image, ENT_COMPAT,'ISO-8859-1', true);


?>
<!DOCTYPE html>
<html lang="nl" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">
</head>
<body>

<form action="/admin/pages/saves/update_teammate.php" method="post" enctype="multipart/form-data">


    <div class="footerEditSection">
        <p class="thickFont">Wijzig team lid <?php echo $pageContent["name"]; ?>.</p>
        <img src="<?php echo $pageContent["image"]; ?>" alt="<?php echo $pageContent["name"]; ?>">

        <p class="descriptionText">Afbeelding</p>
        <input type="file" name="image"><br>
        <p class="descriptionText">Naam</p>
        <input type="text" name="name" value="<?php echo $pageContent["name"]; ?>"
               placeholder="Naam" required><br>
        <p class="descriptionText">Beschrijving</p>
        <textarea rows="8" cols="70%"
                  name="description" placeholder="Beschrijving" required><?php echo $pageContent["description"]; ?></textarea><br>
    </div>

    <div class="footerEditSection">
        <p class="thickFont">Alles klaar?</p>
        <input type="submit" class="submitButton" name="submit" value="update">
        <input type="hidden" name="pageRedirect" value="paginas-home">
        <input type="hidden" name="idTeammate" value="<?php echo $pageContent["id"]; ?>">
    </div>

</form>
</body>
</html>
