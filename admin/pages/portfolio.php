<?php
//Code in dit bestand is gemaakt door Maurice de Jong - MD3A
require_once('../../includes/php/db_connection.php');
$db_id = 1;
$getAllPageContent_sql = $db->prepare('SELECT `playlist_url` FROM `portfolio_url` WHERE `id` = ?');
$getAllPageContent_sql->bindParam(1, $db_id);
$getAllPageContent_sql->execute();

while ($getAllPageContent_result = $getAllPageContent_sql->fetch(PDO::FETCH_OBJ)) {
$pageContent["playlist_url"] = htmlspecialchars($getAllPageContent_result->playlist_url, ENT_COMPAT,'ISO-8859-1', true);
}
?>
<!DOCTYPE html>
<html lang="nl" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">
</head>
<body>


<div class="footerEditSection">
    <p class="thickFont">Portfolio items updaten</p>
    <p>Via dit venster kunt u alle portfolio items updaten op de website. <br>De portfolio items worden van de YouTube
        afspeellijst gehaald en in de website gezet.</p>

    <br>
    <p class="thickFont">Wanneer moet ik updaten?</p>
    <p>U moet updaten bij alle wijzigingen die u maakt aan de afspeellijst of video's in de afspeellist. </p>

    <br>
    <p class="thickFont">Door hieronder op de knop de drukken, update u de porfolio items.</p>
    <a class="button-pink" href="/admin/pages/saves/updatePortfolioItems.php">Update portfolio items</a>
</div>


<form action="/admin/pages/saves/update_portfolio_playlist.php" method="post">
<div class="footerEditSection">
    <p class="thickFont">YouTube afspeellijst instellen</p>
    <p class="descriptionText">YouTube afspeellijst link:</p>
    <input type="text" name="playlist_url" placeholder="YouTube afspeellijst" value="<?php echo $pageContent["playlist_url"]; ?>"><br>
    <input type="submit" class="submitButton" name="submit" value="Updaten">
</div>
</form>

</body>
</html>
