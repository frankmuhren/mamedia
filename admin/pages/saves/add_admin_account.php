<?php
//Code in dit bestand is gemaakt door Maurice de Jong - MD3A

session_start();

$is_logged_in = false;
$logged_in_error = 0;

//    Turn session into variable
if (isset($_SESSION["token"])) {
    $token = $_SESSION["token"];
} else {
    $logged_in_error = $logged_in_error + 1;
}

if ($logged_in_error == 0) {
    //    Database connection
    require_once '../../../includes/php/db_connection.php';

//    Select user with same token and checks if the user credentials are the same as in
    $checkUserLogin_sql = $db->prepare('SELECT `token`, `user_ip` FROM `users` WHERE `token` = ?');
    $checkUserLogin_sql->bindParam(1, $token);
    $checkUserLogin_sql->execute();

    $checkUserLogin_result = $checkUserLogin_sql->fetch(PDO::FETCH_OBJ);

    if (isset($checkUserLogin_result->token) &&
        isset($checkUserLogin_result->user_ip) &&
        isset($_SESSION["token"]) &&
        !empty($checkUserLogin_result->token) &&
        !empty($checkUserLogin_result->user_ip) &&
        !empty($_SESSION["token"]) &&
        $checkUserLogin_result->token == $_SESSION["token"] &&
        hash('sha256', $_SERVER["REMOTE_ADDR"]) == $checkUserLogin_result->user_ip) {
        $is_logged_in = true;
    }
}

if ($logged_in_error != 0 || $is_logged_in == false) {
    if (isset($_SESSION["token"])) {
        $emptyvar = "";
        $user_logout_sql = $db->prepare("UPDATE `users` SET `token`=?,`user_ip`=? WHERE `token`=?");
        $user_logout_sql->bindParam(1, $emptyvar);
        $user_logout_sql->bindParam(2, $emptyvar);
        $user_logout_sql->bindParam(3, $_SESSION["token"]);
        $user_logout_sql->execute();
    }

    $_SESSION["token"] = "";
    session_unset();
    session_destroy();
    header("Location: /login");
    die();
}

if ($is_logged_in == true && $logged_in_error == 0) {


    $form_input_mail = $_POST["add-admin-email"];
    $form_input_password = $_POST["add-admin-password"];

    if (isset($_POST["add-admin-email"]) && isset($_POST["add-admin-password"]) && !empty($_POST["add-admin-email"]) && !empty($_POST["add-admin-password"])) {

        $form_input_mail = strtolower($form_input_mail);
        $form_input_password = hash('sha256', $form_input_password);

        $salt = "&gv#@@Dv6rg^%Fg^g5EF";
        $pepper = "3FxTDg%fFESDFtferfszer";
        $form_input_password = hash('sha256', $salt.$form_input_password.$pepper);


        $create_new_user_sql = $db->prepare("INSERT INTO `users`(`email`, `password`) VALUES (?, ?)");
        $create_new_user_sql->bindParam(1, $form_input_mail);
        $create_new_user_sql->bindParam(2, $form_input_password);
        $create_new_user_sql->execute();

        header("Location: /admin/dashboard.php?alertMessage=Succesvol bijgewerkt&alertMessageColor=green&pageRedirect=paginas-admin-accounts-beheren");

    }


}