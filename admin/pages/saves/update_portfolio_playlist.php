<?php
//Code in dit bestand is gemaakt door Maurice de Jong - MD3A

session_start();

$is_logged_in = false;
$logged_in_error = 0;

ini_set('post_max_size', '51M');

$_GLOBAL["youtubeLinkInputs"] = array("home_videoOverlay_videoURL", "stage_videoURL", "samenwerken_videoURL");
//$_GLOBAL["fileUploadInputs"] = array("home_amsterdammertjes_image", "stage_amsterdammertjes_image");

//    Turn session into variable
if (isset($_SESSION["token"])) {
    $token = $_SESSION["token"];
} else {
    $logged_in_error = $logged_in_error + 1;
}

if ($logged_in_error == 0) {
    //    Database connection
    require_once '../../../includes/php/db_connection.php';

//    Select user with same token and checks if the user credentials are the same as in
    $checkUserLogin_sql = $db->prepare('SELECT `token`, `user_ip` FROM `users` WHERE `token` = ?');
    $checkUserLogin_sql->bindParam(1, $token);
    $checkUserLogin_sql->execute();

    $checkUserLogin_result = $checkUserLogin_sql->fetch(PDO::FETCH_OBJ);

    if (isset($checkUserLogin_result->token) &&
        isset($checkUserLogin_result->user_ip) &&
        isset($_SESSION["token"]) &&
        !empty($checkUserLogin_result->token) &&
        !empty($checkUserLogin_result->user_ip) &&
        !empty($_SESSION["token"]) &&
        $checkUserLogin_result->token == $_SESSION["token"] &&
        hash('sha256', $_SERVER["REMOTE_ADDR"]) == $checkUserLogin_result->user_ip) {
        $is_logged_in = true;
    }
}

if ($logged_in_error != 0 || $is_logged_in == false) {
    if (isset($_SESSION["token"])) {
        $emptyvar = "";
        $user_logout_sql = $db->prepare("UPDATE `users` SET `token`=?,`user_ip`=? WHERE `token`=?");
        $user_logout_sql->bindParam(1, $emptyvar);
        $user_logout_sql->bindParam(2, $emptyvar);
        $user_logout_sql->bindParam(3, $_SESSION["token"]);
        $user_logout_sql->execute();
    }

    $_SESSION["token"] = "";
    session_unset();
    session_destroy();
    header("Location: /login");
    die();
}

if ($is_logged_in == true && $logged_in_error == 0) {






            $stringPos = strpos($_POST["playlist_url"],"list=");
            $replace = substr($_POST["playlist_url"], $stringPos + 5);


            $replace = mb_convert_encoding(
                strip_tags($replace),
                "HTML-ENTITIES",
                "UTF-8");


//            $replace = $value;

            $db_id = 1;
            $update_playlist_url_sql = $db->prepare("UPDATE `portfolio_url` SET `playlist_url`=? WHERE `id`=?");
    $update_playlist_url_sql->bindParam(1, $replace, PDO::PARAM_STR);
    $update_playlist_url_sql->bindParam(2, $db_id);
    $update_playlist_url_sql->execute();





    header("Location: /admin/dashboard.php?alertMessage=Playlist is succesvol bijgewerkt&alertMessageColor=green&pageRedirect=portfolio");

}
