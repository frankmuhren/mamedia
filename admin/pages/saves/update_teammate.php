<?php
//Code in dit bestand is gemaakt door Maurice de Jong - MD3A

session_start();


$is_logged_in = false;
$logged_in_error = 0;

//    Turn session into variable
if (isset($_SESSION["token"])) {
    $token = $_SESSION["token"];
} else {
    $logged_in_error = $logged_in_error + 1;
}

if ($logged_in_error == 0) {
    //    Database connection
    require_once '../../../includes/php/db_connection.php';

//    Select user with same token and checks if the user credentials are the same as in
    $checkUserLogin_sql = $db->prepare('SELECT `token`, `user_ip` FROM `users` WHERE `token` = ?');
    $checkUserLogin_sql->bindParam(1, $token);
    $checkUserLogin_sql->execute();

    $checkUserLogin_result = $checkUserLogin_sql->fetch(PDO::FETCH_OBJ);

    if (isset($checkUserLogin_result->token) &&
        isset($checkUserLogin_result->user_ip) &&
        isset($_SESSION["token"]) &&
        !empty($checkUserLogin_result->token) &&
        !empty($checkUserLogin_result->user_ip) &&
        !empty($_SESSION["token"]) &&
        $checkUserLogin_result->token == $_SESSION["token"] &&
        hash('sha256', $_SERVER["REMOTE_ADDR"]) == $checkUserLogin_result->user_ip) {
        $is_logged_in = true;
    }
}


if ($logged_in_error != 0 || $is_logged_in == false) {
    if (isset($_SESSION["token"])) {
        $emptyvar = "";
        $user_logout_sql = $db->prepare("UPDATE `users` SET `token`=?,`user_ip`=? WHERE `token`=?");
        $user_logout_sql->bindParam(1, $emptyvar);
        $user_logout_sql->bindParam(2, $emptyvar);
        $user_logout_sql->bindParam(3, $_SESSION["token"]);
        $user_logout_sql->execute();
    }

    $_SESSION["token"] = "";
    session_unset();
    session_destroy();
    header("Location: /login");
    die();
}

if ($is_logged_in == true && $logged_in_error == 0) {

    $update_team_lid_sql = $db->prepare("UPDATE `colleague` SET `name`=?,`description`=? WHERE `id`=?");
    $update_team_lid_sql->bindParam(1, $_POST["name"]);
    $update_team_lid_sql->bindParam(2, $_POST["description"]);
    $update_team_lid_sql->bindParam(3, $_POST["idTeammate"]);
    $update_team_lid_sql->execute();


    if (isset($_FILES["image"])) {

            $file = $_FILES["image"];

            $database_url = "/images/personeel/".strtotime("now"). basename($file["name"]);

            $target_dir = "../../../images/personeel/";
            $target_file = $target_dir .strtotime("now"). basename($file["name"]);
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

            $check = getimagesize($file["tmp_name"]);
            if ($check !== false) {
//            echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
//            echo "File is not an image.";
                $uploadOk = 0;
            }


            if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                && $imageFileType != "gif" && $imageFileType != "svg") {
//            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = 0;
            }

            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
//            echo "Sorry, your file was not uploaded.";
                // if everything is ok, try to upload file
            } else {
//            echo $file["tmp_name"]. " - Halalalalalalalalalalalalalalalal";
                if (move_uploaded_file($file["tmp_name"], $target_file)) {
//                echo "The file " . basename($file["name"]) . " has been uploaded.";

                    $dbvar = $_POST["idTeammate"];
                    $selectUploadedFile_sql = $db->prepare('SELECT `image` FROM `colleague` WHERE `id` = ?');
                    $selectUploadedFile_sql->bindParam(1, $dbvar);
                    $selectUploadedFile_sql->execute();
                    $selectUploadedFile_result = $selectUploadedFile_sql->fetch(PDO::FETCH_OBJ);

                    unlink('../../..'.$selectUploadedFile_result->image);

                    $update_content_sql = $db->prepare("UPDATE `colleague` SET `image`=? WHERE `id`=?");
                    $update_content_sql->bindParam(1, $database_url);
                    $update_content_sql->bindParam(2, $dbvar);
                    $update_content_sql->execute();

                } else {
//                echo "Sorry, there was an error uploading your file.";
                }
            }







    }


    header("Location: /admin/dashboard.php?alertMessage=Team lid is succesvol bijgewerkt&alertMessageColor=green&pageRedirect=".htmlentities($_POST["pageRedirect"]));




}