<?php
//Code in dit bestand is gemaakt door Maurice de Jong - MD3A

session_start();

require_once '../../../includes/php/db_connection.php';

$emptyvar = "";
$user_logout_sql = $db->prepare("UPDATE `users` SET `token`=?,`user_ip`=? WHERE `token`=?");
$user_logout_sql->bindParam(1, $emptyvar);
$user_logout_sql->bindParam(2, $emptyvar);
$user_logout_sql->bindParam(3, $_SESSION["token"]);
$user_logout_sql->execute();

$_SESSION["token"] = "";
session_unset();
session_destroy();
header("Location: /login");
die();