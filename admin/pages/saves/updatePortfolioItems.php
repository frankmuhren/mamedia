<?php
//Code in dit bestand is gemaakt door Maurice de Jong - MD3A


session_start();

$is_logged_in = false;
$logged_in_error = 0;


$GLOBALS['projectTitle'] = array();
$GLOBALS['projectDescription'] = array();
$GLOBALS['tumbnailURL'] = array();
$GLOBALS['projectVideoURL'] = array();


//    Database connection
require_once '../../../includes/php/db_connection.php';

$db_id = 1;
$getYouTubePlayListId_sql = $db->prepare('SELECT `playlist_url` FROM `portfolio_url` WHERE `id` = ?');
$getYouTubePlayListId_sql->bindParam(1, $db_id);
$getYouTubePlayListId_sql->execute();
$getYouTubePlayListId_result = $getYouTubePlayListId_sql->fetch(PDO::FETCH_OBJ);

$GLOBALS['youtubePlaylistID'] = $getYouTubePlayListId_result->playlist_url;
//$GLOBALS['youtubePlaylistID'] = "PLAKWHH7jA-2TWhIt9hu9KSMYex_UmgEIX";


$GLOBALS['loadedEveryting'] = false;



//    Turn session into variable
if (isset($_SESSION["token"])) {
    $token = $_SESSION["token"];
} else {
    $logged_in_error = $logged_in_error + 1;
}

if ($logged_in_error == 0) {


//    Select user with same token and checks if the user credentials are the same as in
    $checkUserLogin_sql = $db->prepare('SELECT `token`, `user_ip` FROM `users` WHERE `token` = ?');
    $checkUserLogin_sql->bindParam(1, $token);
    $checkUserLogin_sql->execute();
    $checkUserLogin_result = $checkUserLogin_sql->fetch(PDO::FETCH_OBJ);


    if (isset($checkUserLogin_result->token) &&
        isset($checkUserLogin_result->user_ip) &&
        isset($_SESSION["token"]) &&
        !empty($checkUserLogin_result->token) &&
        !empty($checkUserLogin_result->user_ip) &&
        !empty($_SESSION["token"]) &&
        $checkUserLogin_result->token == $_SESSION["token"] &&
        hash('sha256', $_SERVER["REMOTE_ADDR"]) == $checkUserLogin_result->user_ip) {
        $is_logged_in = true;
    }
}

if ($logged_in_error != 0 || $is_logged_in == false) {
    if (isset($_SESSION["token"])) {
        $emptyvar = "";
        $user_logout_sql = $db->prepare("UPDATE `users` SET `token`=?,`user_ip`=? WHERE `token`=?");
        $user_logout_sql->bindParam(1, $emptyvar);
        $user_logout_sql->bindParam(2, $emptyvar);
        $user_logout_sql->bindParam(3, $_SESSION["token"]);
        $user_logout_sql->execute();
    }

    $_SESSION["token"] = "";
    session_unset();
    session_destroy();
    header("Location: /login");
    die();
}


function putVideoInArray($portfolioItemArray) {
    for ($x = 0; $x <= count($portfolioItemArray) -1; $x++) {
        array_push($GLOBALS['projectTitle'], $portfolioItemArray[$x]["snippet"]["title"]);
        array_push($GLOBALS['projectDescription'], $portfolioItemArray[$x]["snippet"]["description"]);
        array_push($GLOBALS['tumbnailURL'], $portfolioItemArray[$x]["snippet"]["thumbnails"]["high"]["url"]);
        array_push($GLOBALS['projectVideoURL'], $portfolioItemArray[$x]["snippet"]["resourceId"]["videoId"]);
    }
}




if ($is_logged_in == true && $logged_in_error == 0) {


    $get_youtube_videos = curl_init ("https://www.googleapis.com/youtube/v3/playlistItems?key=AIzaSyA15BCyws-VW0SXKPUsvt8M1YtKJchAyqU&part=snippet&maxresults=50&playlistId=". $GLOBALS['youtubePlaylistID']);
    curl_setopt($get_youtube_videos, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($get_youtube_videos, CURLOPT_SSL_VERIFYPEER, true);
    curl_setopt($get_youtube_videos, CURLOPT_RETURNTRANSFER, true);

    $get_youtube_videos_results = curl_exec($get_youtube_videos);
    $get_youtube_videos_results = json_decode($get_youtube_videos_results, true);

    putVideoInArray($get_youtube_videos_results["items"]);

    $nextPageCode = $get_youtube_videos_results["nextPageToken"];
    $length = ceil($get_youtube_videos_results["pageInfo"]["totalResults"] / $get_youtube_videos_results["pageInfo"]["resultsPerPage"]);

    if (isset($get_youtube_videos_results["nextPageToken"])) {
        $nextPageCode = $get_youtube_videos_results["nextPageToken"];
    } else {
        $GLOBALS['loadedEveryting'] = true;
    }

    while (!$GLOBALS['loadedEveryting']) {

        $get_youtube_videos = curl_init("https://www.googleapis.com/youtube/v3/playlistItems?key=AIzaSyA15BCyws-VW0SXKPUsvt8M1YtKJchAyqU&part=snippet&maxresults=50&playlistId=".$GLOBALS['youtubePlaylistID']."&pageToken=". $nextPageCode);
        curl_setopt($get_youtube_videos, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($get_youtube_videos, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($get_youtube_videos, CURLOPT_RETURNTRANSFER, true);

        $get_youtube_videos_results = curl_exec($get_youtube_videos);
        $get_youtube_videos_results = json_decode($get_youtube_videos_results, true);

        if (isset($get_youtube_videos_results["nextPageToken"])) {
            $nextPageCode = $get_youtube_videos_results["nextPageToken"];
        } else {
            $GLOBALS['loadedEveryting'] = true;
        }

        putVideoInArray($get_youtube_videos_results["items"]);

    }

    $deleteAllPortfolioItems_sql = $db->prepare('DELETE FROM `portfolio_items`');
    $deleteAllPortfolioItems_sql->execute();


    for ($x = 0; $x <= count($GLOBALS['projectTitle']) -1; $x++) {
        $db_project_title = urlencode($GLOBALS['projectTitle'][$x]);
        $db_project_description = $GLOBALS['projectDescription'][$x];
        $db_project_tumbnail_url = $GLOBALS['tumbnailURL'][$x];
        $db_project_video_url = $GLOBALS['projectVideoURL'][$x];

//        $db_project_title = mb_convert_encoding(
//            strip_tags($GLOBALS['projectTitle'][$x]),
//            "HTML-ENTITIES",
//            "UTF-8"
//        );
//        $db_project_description = mb_convert_encoding(
//            strip_tags($GLOBALS['projectDescription'][$x]),
//            "HTML-ENTITIES",
//            "UTF-8"
//        );
//        $db_project_tumbnail_url = mb_convert_encoding(
//            strip_tags($GLOBALS['tumbnailURL'][$x]),
//            "HTML-ENTITIES",
//            "UTF-8"
//        );
//        $db_project_video_url = mb_convert_encoding(
//            strip_tags($GLOBALS['projectVideoURL'][$x]),
//            "HTML-ENTITIES",
//            "UTF-8"
//        );

        $insertPortfolioItems_sql = $db->prepare('INSERT INTO `portfolio_items`(`title`, `description`, `tumbnail_url`, `video_url`) VALUES (?,?,?,?)');
        $insertPortfolioItems_sql->bindParam(1, $db_project_title);
        $insertPortfolioItems_sql->bindParam(2, $db_project_description);
        $insertPortfolioItems_sql->bindParam(3, $db_project_tumbnail_url);
        $insertPortfolioItems_sql->bindParam(4, $db_project_video_url);
        $insertPortfolioItems_sql->execute();
    }

    header("Location: /admin/dashboard.php?alertMessage=Portfolio items zijn bijgewerkt&alertMessageColor=green&pageRedirect=portfolio");


}

