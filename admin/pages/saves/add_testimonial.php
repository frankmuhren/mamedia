<?php

//Code in dit bestand is gemaakt door Maurice de Jong - MD3A

session_start();


$is_logged_in = false;
$logged_in_error = 0;

//    Turn session into variable
if (isset($_SESSION["token"])) {
    $token = $_SESSION["token"];
} else {
    $logged_in_error = $logged_in_error + 1;
}

if ($logged_in_error == 0) {
    //    Database connection
    require_once '../../../includes/php/db_connection.php';

//    Select user with same token and checks if the user credentials are the same as in
    $checkUserLogin_sql = $db->prepare('SELECT `token`, `user_ip` FROM `users` WHERE `token` = ?');
    $checkUserLogin_sql->bindParam(1, $token);
    $checkUserLogin_sql->execute();

    $checkUserLogin_result = $checkUserLogin_sql->fetch(PDO::FETCH_OBJ);

    if (isset($checkUserLogin_result->token) &&
        isset($checkUserLogin_result->user_ip) &&
        isset($_SESSION["token"]) &&
        !empty($checkUserLogin_result->token) &&
        !empty($checkUserLogin_result->user_ip) &&
        !empty($_SESSION["token"]) &&
        $checkUserLogin_result->token == $_SESSION["token"] &&
        hash('sha256', $_SERVER["REMOTE_ADDR"]) == $checkUserLogin_result->user_ip) {
        $is_logged_in = true;
    }
}


if ($logged_in_error != 0 || $is_logged_in == false) {
    if (isset($_SESSION["token"])) {
        $emptyvar = "";
        $user_logout_sql = $db->prepare("UPDATE `users` SET `token`=?,`user_ip`=? WHERE `token`=?");
        $user_logout_sql->bindParam(1, $emptyvar);
        $user_logout_sql->bindParam(2, $emptyvar);
        $user_logout_sql->bindParam(3, $_SESSION["token"]);
        $user_logout_sql->execute();
    }

    $_SESSION["token"] = "";
    session_unset();
    session_destroy();
    header("Location: /login");
    die();
}

if ($is_logged_in == true && $logged_in_error == 0) {

    $testimonial = mb_convert_encoding(
        strip_tags($_POST["testimonial"]),
        "HTML-ENTITIES",
        "UTF-8"
    );

    $insert_testimonial_sql = $db->prepare("INSERT INTO `testimonials`(`testimonial_value`, `pagina`) VALUES (?, ?)");
    $insert_testimonial_sql->bindParam(1, $testimonial);
    $insert_testimonial_sql->bindParam(2, $_POST["pagina"]);
    $insert_testimonial_sql->execute();




    header("Location: /admin/dashboard.php?alertMessage=Testimonial is succesvol toegevoegd&alertMessageColor=green&pageRedirect=".htmlentities($_POST["pageRedirect"]));

}