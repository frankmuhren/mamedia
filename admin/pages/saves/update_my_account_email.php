<?php
//Code in dit bestand is gemaakt door Maurice de Jong - MD3A

session_start();

$is_logged_in = false;
$logged_in_error = 0;

//    Turn session into variable
if (isset($_SESSION["token"])) {
    $token = $_SESSION["token"];
} else {
    $logged_in_error = $logged_in_error + 1;
}

if ($logged_in_error == 0) {
    //    Database connection
    require_once '../../../includes/php/db_connection.php';

//    Select user with same token and checks if the user credentials are the same as in
    $checkUserLogin_sql = $db->prepare('SELECT `token`, `user_ip` FROM `users` WHERE `token` = ?');
    $checkUserLogin_sql->bindParam(1, $token);
    $checkUserLogin_sql->execute();

    $checkUserLogin_result = $checkUserLogin_sql->fetch(PDO::FETCH_OBJ);

    if (isset($checkUserLogin_result->token) &&
        isset($checkUserLogin_result->user_ip) &&
        isset($_SESSION["token"]) &&
        !empty($checkUserLogin_result->token) &&
        !empty($checkUserLogin_result->user_ip) &&
        !empty($_SESSION["token"]) &&
        $checkUserLogin_result->token == $_SESSION["token"] &&
        hash('sha256', $_SERVER["REMOTE_ADDR"]) == $checkUserLogin_result->user_ip) {
        $is_logged_in = true;
    }
}


if ($logged_in_error != 0 || $is_logged_in == false) {
    if (isset($_SESSION["token"])) {
        $emptyvar = "";
        $user_logout_sql = $db->prepare("UPDATE `users` SET `token`=?,`user_ip`=? WHERE `token`=?");
        $user_logout_sql->bindParam(1, $emptyvar);
        $user_logout_sql->bindParam(2, $emptyvar);
        $user_logout_sql->bindParam(3, $_SESSION["token"]);
        $user_logout_sql->execute();
    }

    $_SESSION["token"] = "";
    session_unset();
    session_destroy();
    header("Location: /login");
    die();
}

if ($is_logged_in == true && $logged_in_error == 0) {


    if (isset($_POST["oldEmail"]) && !empty($_POST["oldEmail"]) && isset($_POST["newEmail"]) && !empty($_POST["newEmail"])) {

        $oldEmail = $_POST["oldEmail"];

        $select_user_old_email_sql = $db->prepare("SELECT `email` FROM `users` WHERE `email`=?");
        $select_user_old_email_sql->bindParam(1, $oldEmail);
        $select_user_old_email_sql->execute();
        $select_user_old_email_result = $select_user_old_email_sql->fetch(PDO::FETCH_OBJ);


    if (isset($select_user_old_email_result) && !empty($select_user_old_email_result) && $select_user_old_email_result->email == $oldEmail) {
        $newEmail = $_POST["newEmail"];

        if (filter_var($newEmail, FILTER_VALIDATE_EMAIL)) {

            $emptyvar = "";
            $update_user_email_sql = $db->prepare("UPDATE `users` SET `email`=?, `token`=?, `user_ip`=? WHERE `token`=?");
            $update_user_email_sql->bindParam(1, $newEmail);
            $update_user_email_sql->bindParam(2, $emptyvar);
            $update_user_email_sql->bindParam(3, $emptyvar);
            $update_user_email_sql->bindParam(4, $_SESSION["token"]);
            $update_user_email_sql->execute();

            header("Location: /admin/dashboard.php?alertMessage=E-mail is succesvol bijgewerkt&alertMessageColor=green&pageRedirect=paginas-admin-update-account");

        } else {

            header("Location: /admin/dashboard.php?alertMessage=Ongeldige e-mail&alertMessageColor=yellow&pageRedirect=paginas-admin-update-account");

        }
    } else {

        header("Location: /admin/dashboard.php?alertMessage=Admin met dit email adres niet gevonden&alertMessageColor=red&pageRedirect=paginas-admin-update-account");

    }

    }

}