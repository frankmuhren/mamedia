<!--Code in dit bestand is gemaakt door Maurice de Jong - MD3A-->
<!DOCTYPE html>
<html lang="nl" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">
</head>
<body>

<form action="/admin/pages/saves/update_my_account_email.php" method="post">

    <div class="footerEditSection">
        <p class="thickFont">Uw e-mailadres veranderen</p>
        <input type="email" name="oldEmail" placeholder="Oud e-mailadres" required><br>
        <input type="email" name="newEmail" placeholder="Nieuw e-mailadres" required><br>
        <input type="submit" class="submitButton" name="submit" value="update">
    </div>
</form>

<form action="/admin/pages/saves/update_my_account_password.php" method="post">

    <div class="footerEditSection">
        <p class="thickFont">Uw wachtwoord veranderen</p>
        <input type="password" name="oldPassword" value="" placeholder="Oud wachtwoord" required><br>
        <input type="password" name="newPassword" value="" placeholder="Nieuw wachtwoord" required><br>
        <input type="submit" class="submitButton" name="submit" value="update">
    </div>
</form>

</body>
</html>
