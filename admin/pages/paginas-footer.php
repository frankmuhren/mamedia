<?php
require_once('../../includes/php/db_connection.php');
$page_id = "footer";
$getAllPageContent_sql = $db->prepare('SELECT `title`, `value` FROM `page_content` WHERE `page_id` = ?');
$getAllPageContent_sql->bindParam(1, $page_id);
$getAllPageContent_sql->execute();

while ($getAllPageContent_result = $getAllPageContent_sql->fetch(PDO::FETCH_OBJ)) {
    $pageContentFooter[$getAllPageContent_result->title] = htmlspecialchars($getAllPageContent_result->value, ENT_COMPAT,'ISO-8859-1', true);
}

?>
<!DOCTYPE html>
<html lang="nl" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">
</head>
<body>
<form action="/admin/pages/saves/update_page_content.php" method="post">

    <div class="footerEditSection">
        <p class="thickFont">Verander de contact gegevens in de footer.</p>
        <p class="descriptionText">Straat</p>
        <input type="text" name="footer_street" value="<?php echo $pageContentFooter["footer_street"]; ?>"
               placeholder="straat"><br>
        <p class="descriptionText">Postcode1</p>
        <input type="text" name="footer_postalcode" value="<?php echo $pageContentFooter["footer_postalcode"]; ?>"
               placeholder="postcode1">
    </div>


    <div class="footerEditSection">
        <p class="thickFont">Verander de de postAdress gegevens in de footer.</p>
        <p class="descriptionText">Naam</p>
        <input type="text" name="footer_name" value="<?php echo $pageContentFooter["footer_name"]; ?>"
               placeholder="naam"><br>
        <p class="descriptionText">Tav</p>
        <input type="text" name="footer_tav" value="<?php echo $pageContentFooter["footer_tav"]; ?>"
               placeholder="tav"><br>
        <p class="descriptionText">Postbus</p>
        <input type="text" name="footer_mailbox" value="<?php echo $pageContentFooter["footer_mailbox"]; ?>"
               placeholder="postbus"><br>
        <p class="descriptionText">Postcode2</p>
        <input type="text" name="footer_postalcode2" value="<?php echo $pageContentFooter["footer_postalcode2"]; ?>"
               placeholder="postcode2">
    </div>

    <div class="footerEditSection">
        <p class="thickFont">Verander de telefoon en en email gegevens in de footer.</p>
        <p class="descriptionText">Telefoon</p>
        <input type="text" name="footer_phonenumber" value="<?php echo $pageContentFooter["footer_phonenumber"]; ?>"
               placeholder="telefoon"><br>
        <p class="descriptionText">E-mailadres</p>
        <input type="text" name="footer_email" value="<?php echo $pageContentFooter["footer_email"]; ?>"
               placeholder="E-mailadres">
    </div>

    <!--Begin Maurice-->
    <div class="footerEditSection">
        <p class="thickFont">Verander de social media links in de footer.</p>
        <p class="descriptionText">Social media YouTube link</p>
        <input type="text" name="footer_youtube_link" value="<?php echo $pageContentFooter["footer_youtube_link"]; ?>"
               placeholder="YouTube link"><br>
        <p class="descriptionText">Social media Facebook link</p>
        <input type="text" name="footer_facebook_link" value="<?php echo $pageContentFooter["footer_facebook_link"]; ?>"
               placeholder="Facebook link"><br>
        <p class="descriptionText">Social media Instagram link</p>
        <input type="text" name="footer_instagram_link"
               value="<?php echo $pageContentFooter["footer_instagram_link"]; ?>" placeholder="Instagram link">
    </div>
    <!-- Eind Maurice-->

    <div class="footerEditSection">
        <p class="thickFont">Alles klaar?</p>
        <input type="submit" class="submitButton" name="submit" value="update">
        <input type="hidden" name="pageRedirect" value="paginas-footer">
    </div>

</form>

</body>
</html>
