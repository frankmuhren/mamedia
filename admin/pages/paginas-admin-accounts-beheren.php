<!--Code in dit bestand is gemaakt door Maurice de Jong - MD3A-->
<!DOCTYPE html>
<html lang="nl" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">
</head>
<body>

<?php

require_once '../../includes/php/db_connection.php';
$getAllUserMails_sql = $db->prepare('SELECT `id`, `email` FROM `users`');
$getAllUserMails_sql->execute();

?>

<div class="footerEditSection">
    <p class="thickFont">Admin account overzicht</p>
    <table>
        <tr>
            <th>E-mailadres</th>
            <th>Actie</th>
        </tr>
        <?php

        while ($getAllUserMails_result = $getAllUserMails_sql->fetch(PDO::FETCH_OBJ)) {
            echo '<tr><td>' . htmlspecialchars($getAllUserMails_result->email, ENT_COMPAT,'ISO-8859-1', true) . '</td><td class="color-red"><a href="/admin/pages/saves/delete_admin_account.php?adminaccountid=' . urlencode($getAllUserMails_result->id) . '">Verwijderen</a></td></tr>';
        }

        ?>

    </table>
</div>


<form action="/admin/pages/saves/add_admin_account.php" method="post">

    <div class="footerEditSection">
        <p class="thickFont">Admin account toevoegen</p>
        <input type="text" name="add-admin-email" placeholder="E-mailadres" required><br>
        <input type="password" name="add-admin-password" placeholder="Wachtoord" required><br>
        <input type="submit" class="submitButton" name="submit" value="Account toevoegen">
    </div>

</form>
</body>
</html>
