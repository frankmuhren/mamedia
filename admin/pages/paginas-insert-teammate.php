<?php
//Code in dit bestand is gemaakt door Maurice de Jong - MD3A

require_once('../../includes/php/db_connection.php');

?>
<!DOCTYPE html>
<html lang="nl" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">
</head>
<body>

<form action="/admin/pages/saves/add_teammate.php" method="post" enctype="multipart/form-data">


    <div class="footerEditSection">
        <p class="thickFont">Team team lid toevoegen.</p>

        <p class="descriptionText">Afbeelding</p>
        <input type="file" name="image" required><br>
        <p class="descriptionText">Naam</p>
        <input type="text" name="name"
               placeholder="Naam" required><br>
        <p class="descriptionText">Beschrijving</p>
        <textarea rows="8" cols="70%"
                  name="description" placeholder="Beschrijving" required></textarea><br>
    </div>

    <div class="footerEditSection">
        <p class="thickFont">Alles klaar?</p>
        <input type="submit" class="submitButton" name="submit" value="Toevoegen">
        <input type="hidden" name="pageRedirect" value="paginas-home">
    </div>

</form>
</body>
</html>
