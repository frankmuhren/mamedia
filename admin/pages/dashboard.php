<!DOCTYPE html>
<html lang="nl" dir="ltr">
  <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
  </head>
  <body>

<div class="guideSection">
<p class="thickFont">Hallo, welkom in het admin paneel voor Mamedia.</p>
  <p class="descriptionText">Laten we starten...</p>
</div>

<div class="guideSection">
<p class="thickFont">Het admin paneel geeft u de mogelijkheid om de website van Mamedia te beheren en te veranderen.</p>
  <p class="descriptionText">U kunt:</p>
  <ul>
  <li>Teksten, links en images veranderen.</li>
  <li>Medewerkers toevoegen / verwijderen.</li>
  <li>Projecten updaten.</li>
  <li>Admin gebruikers beheren.</li>
</ul>
<p class="thickFont">Teksten, links en images veranderen.</p>
  <p class="descriptionText">In het handige overzicht links van de pagina staat het kopje "Pagina's". Als je daar op klikt laat het een handig dropdown menu zien met alle pagina's van de website erop.
  Dit zijn al de pagina's die aangepast kunnen worden.</p>
  <p class="descriptionText">Elke pagina is voorzien van een aantal inputvelden. Elke image, stuk tekst, titel of link op de website is gekoppeld met deze inputvelden.
  Als u bijvoorbeeld een stuk tekst op de website wilt veranderen navigeert u eerst naar de juiste pagina in dit CMS, dan zoekt u het juiste inputveld op. Nadat u de veranderingen heb getypt kunt u met de button onderaan de pagina de website updaten.</p>
  <p class="descriptionText">De webpagina schaalt automatisch mee met de tekst, u hoeft dus geen zorgen te maken over lange stukken tekst die overlappen met andere stukken tekst.</p>
  <p class="thickFont">Medewerkers toevoegen / verwijderen.</p>
  <p class="descriptionText">Navigeer naar de landingspage in het CMS, onder het kopje Pagina's. Onderaan die pagina is een sectie met werknemers. Deze zijn net zoals teksten en images makkelijk aanpasbaar. Als u blij bent met de aanpassingen, klikt u op de knop onderaan de pagina om het te updaten. </p>
  <p class="descriptionText">Het is belangrijk om te weten dat het CMS geen crop feature bevat. Als admin moet u ervoor zorgen dat al de images die u upload de juiste verhoudingen hebben. Anders zijn de images die worden geupload niet even lang op de website.</p>
  <p class="thickFont">Projecten updaten.</p>
  <p class="descriptionText">Projecten worden geüpdate via youtube. Om een project toe te voegen of aan te passen navigeert u naar het kopje "projecten" onder "pagina's".
    De portfolio items worden van de YouTube afspeellijst gehaald en in de website gezet.</p>
          <p class="descriptionText"><a href="https://www.youtube.com/watch?v=IyrAbs7eBA4&list=PLAKWHH7jA-2TWhIt9hu9KSMYex_UmgEIX">https://www.youtube.com/watch?v=IyrAbs7eBA4&list=PLAKWHH7jA-2TWhIt9hu9KSMYex_UmgEIX</a></p>
          <p class="descriptionText">Zodra er veranderingen zijn gemaakt aan de afspeellijst klikt u op de knop "update portfolio items" en word dat automatisch voor u gedaan.</p>
  <p class="thickFont">Admin gebruikers</p>
  <p class="descriptionText">Bij het kopje "Gebruikers" staat er een overzicht met al de admin accounts. Via het overzicht kunt u net zoals het updaten van tekst, images, en plaatjes accounts toevoegen of verwijderen.</p>
  <p class="descriptionText">Alleen accounts die in het overview staan hebben toegang om in te loggen via het CMS login systeem. Het CMS zorgt ervoor dat er atlijd tenminste een account overblijft. Hierdoor is het niet mogelijk om de medewerkers van Mamedia buiten te sluiten van het CMS.</p>
</div>


  </body>
</html>
