<?php
//Code in dit bestand is gemaakt door Maurice de Jong - MD3A
//Date: 25-4-2019

session_start();

$is_logged_in = false;
$logged_in_error = 0;

//    Turn session into variable
if (isset($_SESSION["token"])) {
    $token = $_SESSION["token"];
} else {
    $logged_in_error = $logged_in_error + 1;
}

if ($logged_in_error == 0) {
    //    Database connection
    require_once '../includes/php/db_connection.php';

//    Select user with same token and checks if the user credentials are the same as in
    $checkUserLogin_sql = $db->prepare('SELECT `token`, `user_ip` FROM `users` WHERE `token` = ?');
    $checkUserLogin_sql->bindParam(1, $token);
    $checkUserLogin_sql->execute();

    $checkUserLogin_result = $checkUserLogin_sql->fetch(PDO::FETCH_OBJ);

    if (isset($checkUserLogin_result->token) &&
        isset($checkUserLogin_result->user_ip) &&
        isset($_SESSION["token"]) &&
        !empty($checkUserLogin_result->token) &&
        !empty($checkUserLogin_result->user_ip) &&
        !empty($_SESSION["token"]) &&
        $checkUserLogin_result->token == $_SESSION["token"] &&
        hash('sha256', $_SERVER["REMOTE_ADDR"]) == $checkUserLogin_result->user_ip) {
        $is_logged_in = true;
    }
}

if ($logged_in_error != 0 || $is_logged_in == false) {
    if (isset($_SESSION["token"])) {
        $emptyvar = "";
        $user_logout_sql = $db->prepare("UPDATE `users` SET `token`=?,`user_ip`=? WHERE `token`=?");
        $user_logout_sql->bindParam(1, $emptyvar);
        $user_logout_sql->bindParam(2, $emptyvar);
        $user_logout_sql->bindParam(3, $_SESSION["token"]);
        $user_logout_sql->execute();
    }

    $_SESSION["token"] = "";
    session_unset();
    session_destroy();
    header("Location: /login");
    die();
}

if ($is_logged_in == true && $logged_in_error == 0) {

    ?>
    <!DOCTYPE html>
    <html lang="nl" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta name="robots" content="noindex, nofollow">
        <meta name="application-name" content="MaMedia">
        <link rel="stylesheet" href="/admin/css/dashboard.css">
        <link rel="stylesheet" href="/admin/css/page-footer.css">
        <link rel="stylesheet" href="/admin/css/nav.css">
        <link rel="stylesheet" href="/admin/css/admin-accounts-beheren.css">

        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <title>MaMedia</title>
        <meta name="description" content="-">
        <meta name="keywords" content="-">
    </head>
    <body>

    <nav>
        <ul>
            <li id="dashboard">Dashboard</li>
            <li class="nav-dropdown-trigger">Pagina's</li>
            <ul id="paginas-dropdown" class="nav-dropdown">
                <li id="paginas-home">Homepage</li>
                <li id="paginas-stage">Stage</li>
                <li id="paginas-samenwerken">Samenwerken</li>
                <li id="paginas-footer">Footer</li>
            </ul>
            <li id="portfolio">Portfolio</li>
            <li id="formulieren">Formulieren</li>
            <li class="nav-dropdown-trigger">Admin</li>
            <ul class="nav-dropdown">
                <li id="paginas-admin-mijn-account">Mijn account</li>
                <li id="paginas-admin-accounts-beheren">Accounts beheren</li>
            </ul>
            <a href="/admin/pages/saves/logout.php">
                <li class="color-red background-color-red-hover">Uitloggen</li>
            </a>
        </ul>
    </nav>

    <main>
        <div class="alert" id="alert"></div>

        <div id="cms-content-view"></div>

    </main>
    <?php
    $alertMessage = "-";
    $alertMessageColor = "-";
    $pageRedirect = "-";

    if (isset($_GET["alertMessage"]) && isset($_GET["alertMessageColor"])) {
        $alertMessage = htmlentities($_GET["alertMessage"]);
        $alertMessageColor = htmlentities($_GET["alertMessageColor"]);
    }

    if (isset($_GET["pageRedirect"])) {
        $pageRedirect = htmlentities($_GET["pageRedirect"]);
    }

    $urlCommunicationArray = array(
        "alertMessage" => $alertMessage,
        "alertMessageColor" => $alertMessageColor,
        "pageRedirect" => $pageRedirect
    );
    ?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        var urlCommunication = JSON.parse('<?php echo json_encode($urlCommunicationArray); ?>');
    </script>
    <script src="js/navigation.js"></script>
    <script src="js/alert.js"></script>
    </body>
    </html>
<?php } ?>
