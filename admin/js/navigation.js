/**
 * Code in dit bestand is gemaakt door Maurice de Jong - MD3A
 * Date: 25-4-2019
 **/
$(document).ready(function () {
    $loaded = 0;
    $navClickPosition = -1;

    $dropDownNav = function ($navPosition) {
        if ($navClickPosition !== $navPosition) {
            $("nav ul ul").slideUp();
            $('.nav-dropdown').eq($navPosition).slideToggle();
            $navClickPosition = $navPosition;
        }
    };

    $loadContentInView = function ($url) {
        $.get("pages/" + $url, function (data) {
            $("#cms-content-view").fadeOut(function () {
                $("#cms-content-view").html(data);
            }).fadeIn();
        });
        if ($loaded == 1) {
            $('#alert').fadeOut();
        }
    };


    //Navigation dropdown
    $('.nav-dropdown-trigger').click(function () {
        $dropDownNav($('.nav-dropdown-trigger').index(this));
        $('.nav-dropdown-trigger').removeClass("selected");
        $(this).addClass("selected");
    });

    $changeTeamMate = function($id) {
        $loadContentInView("paginas-wijzig-teammate.php?id="+$id);
    };


    $('nav li').click(function () {
        if ($(this.element).hasClass("nav-dropdown")) {

        } else {
            $('#alert').fadeOut();
        }
    });


//  Navigation
//  If user clicks on menu item. The cms loads that content

    //Dashboard
    $('#dashboard').click(function () {
        $loadContentInView("dashboard.php");
    });


    //Paginas
    $('#paginas-home').click(function () {
        $loadContentInView("paginas-home.php");
    });

    $('#paginas-stage').click(function () {
        $loadContentInView("paginas-stage.php");
    });

    $('#paginas-samenwerken').click(function () {
        $loadContentInView("paginas-samenwerken.php");
    });

    $('#paginas-footer').click(function () {
        $loadContentInView("paginas-footer.php");
    });



    //Portfolio
    $('#portfolio').click(function () {
        $loadContentInView("portfolio.php");
    });



    //Testimonial
    $('#formulieren').click(function () {
        $loadContentInView("formulieren.php");
    });





    // Admin menu
    $('#paginas-admin-mijn-account').click(function () {
        $loadContentInView("paginas-admin-update-account.php");
    });

    $('#paginas-admin-accounts-beheren').click(function () {
        $loadContentInView("paginas-admin-accounts-beheren.php");
    });




    //Start page
    if (urlCommunication.pageRedirect != "-") {
        $loadContentInView(urlCommunication.pageRedirect + ".php");
    } else {
        $loadContentInView("dashboard.php");
    }






    $loaded = 1;
});
