/**
 * Code in dit bestand is gemaakt door Maurice de Jong - MD3A
 * Date: 17-5-2019
 **/
$(document).ready(function () {

    if (urlCommunication.alertMessage != "-" && urlCommunication.alertMessageColor != "-") {
        $('#alert').text(urlCommunication.alertMessage).addClass("background_color_alert_" + urlCommunication.alertMessageColor).css("padding", "10px 0");
    }

});