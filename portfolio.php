<?php
/**
 * Code in dit bestand is gemaakt door Maurice de Jong - MD3A
 * Date: 9-4-2019
 * Time: 09:17
 */

require_once "includes/php/db_connection.php";

$title = "Item niet gevonden";
$description = "-";
$video_url = "-";

if (isset($_GET["id"])){
    $id = preg_replace('/\\.[^.\\s]{3,4}$/', '', $_GET["id"]);

    $getPortfolioItems_sql = $db->prepare('SELECT * FROM portfolio_items WHERE video_url=?');
    $getPortfolioItems_sql->bindParam( 1 , $id);
    $getPortfolioItems_sql->execute();

//Get the data from database and put it in a variable
    $getPortfolioItems_result = $getPortfolioItems_sql->fetch( PDO::FETCH_OBJ );

    if ($id != null && $getPortfolioItems_result != null) {
        $title = $getPortfolioItems_result->title;
        $description = $getPortfolioItems_result->description;
        $video_url = $getPortfolioItems_result->video_url;
    }
} else {
    $id = null;
}

?>

<!DOCTYPE html>
<html lang="nl" dir="ltr">
<head>
    <?php include_once("includes/metatags.php"); ?>
    <link rel="canonical" href="https://<?php echo nl2br(htmlspecialchars($_SERVER['SERVER_NAME'], ENT_COMPAT,'ISO-8859-1', true)); ?>/portfolio/<?php echo nl2br(htmlspecialchars($video_url, ENT_COMPAT,'ISO-8859-1', true)); ?>" />
    <meta name="application-name" content="MaMedia">
    <title>MaMedia - <?php echo nl2br(htmlspecialchars(urldecode($title), ENT_COMPAT,'ISO-8859-1', true)); ?></title>
    <meta name="description" content="<?php echo nl2br(htmlspecialchars($description, ENT_COMPAT,'ISO-8859-1', true)); ?>">
    <meta name="keywords" content="mamedia, mediacollege, amsterdam, stagiair, videoproductie, animaties, grafische vormgeving, webdesign">
</head>
<body>
<?php include_once("includes/nav.php"); ?>

<div class="portfolio_container">
    <div class="mid-container">
        <div class="full_Width-Title"><?php echo nl2br(htmlspecialchars(urldecode($title), ENT_COMPAT,'ISO-8859-1', true)); ?></div>
        <div class="one-two">
            <div class="portfolio_videoContainer">
                <iframe src="https://www.youtube-nocookie.com/embed/<?php echo nl2br(htmlspecialchars($video_url, ENT_COMPAT,'ISO-8859-1', true)); ?>?controls=0&showinfo=0&autoplay=1&mute=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>

        <div class="one-two">
            <div class="middle_Text padding-50">
                <?php echo nl2br(htmlspecialchars($description, ENT_COMPAT,'ISO-8859-1', true)); ?>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="projectLibary_container">

    <div class="mid-container arrow-Holder">
        <div class="arrow arrow-left" id="arrow-left"></div>
        <div class="arrow" id="arrow-right"></div>

        <div class="one-two-portfolio">

<!--Portfolio items will be showed here-->

        </div>

        <div class="clear"></div>
    </div>

</div>

<?php include_once("includes/footer.php");


$projectItemVideoURL = array();
$projectTitle = array();
$tumbnailURL = array();
$gradientColorArray = array();

$getAllPortfolioItems_sql = $db->prepare('SELECT * FROM portfolio_items ORDER BY RAND()');
$getAllPortfolioItems_sql->execute();

while($getAllPortfolioItems_result = $getAllPortfolioItems_sql->fetch( PDO::FETCH_OBJ )) {
    array_push($projectItemVideoURL, htmlentities($getAllPortfolioItems_result->video_url));
    array_push($projectTitle, htmlentities($getAllPortfolioItems_result->title));
    array_push($tumbnailURL, $getAllPortfolioItems_result->tumbnail_url);

    switch (rand (1, 3)) {
        case 1:
            $gradientColor = "gradient-black_blue";
            break;
        case 2:
            $gradientColor = "gradient-blue_purple";
            break;
        case 3:
            $gradientColor = "gradient-purple_black";
            break;
    }

    array_push($gradientColorArray, $gradientColor);
}

$project_items = array(
    "projectItemVideoURL" => $projectItemVideoURL,
    "projectTitle" => $projectTitle,
    "tumbnailURL" => $tumbnailURL,
    "gradient_color" => $gradientColorArray
);

?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    // var portfolioResults = JSON.parse('    {' +
    //     '        "projectItemID": ["1", "2", "3", "4", "5", "6", "7"],' +
    //     '        "projectTitle": ["1", "2", "3", "4", "5", "6", "7"],' +
    //     '        "tumbnailURL": ["http://localhost/images/tumb.jpg", "http://localhost/images/tumb.jpg", "http://localhost/images/tumb.jpg", "http://localhost/images/tumb.jpg", "http://localhost/images/tumb.jpg", "http://localhost/images/tumb.jpg", "http://localhost/images/tumb.jpg"]' +
    //     '    }');

    var portfolioResults = JSON.parse('<?php echo json_encode($project_items); ?>');
</script>
<script src="/js/portfolioItems.js"></script>
<script src="/js/hamburgerMenu.js"></script>
</body>
</html>