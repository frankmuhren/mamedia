<?php
/**
 * Front-end in dit bestand is gemaakt door Frank Muhren - MD3A
 * PHP in dit bestand is gemaakt door Maurice de Jong - MD3A
 * Date: 9-4-2019
 * Time: 09:17
 */
//  Begin Maurice
require_once('includes/php/db_connection.php');

$page_id = "home";
$getAllPageContent_sql = $db->prepare('SELECT `title`, `value` FROM `page_content` WHERE `page_id` = ?');
$getAllPageContent_sql->bindParam(1, $page_id);
$getAllPageContent_sql->execute();

while ($getAllPageContent_result = $getAllPageContent_sql->fetch(PDO::FETCH_OBJ)) {
    $pageContent[$getAllPageContent_result->title] = nl2br(htmlspecialchars(strip_tags($getAllPageContent_result->value), ENT_COMPAT,'ISO-8859-1', true));
}

//  Eind Maurice
?>

<!DOCTYPE html>
<html lang="nl" dir="ltr">
<head>
  <?php include_once("includes/metatags.php"); ?>
    <link rel="canonical" href="https://<?php echo htmlentities($_SERVER['SERVER_NAME']); ?>/" />
  <meta name="application-name" content="Mamedia">
  <!-- <link rel="stylesheet" href="css/landingspage.css"> -->
  <title>MaMedia - Mediacollege Amsterdam</title>
  <meta name="description" content="De diensten van Mamedia bestaan hoofdzakelijk uit videoproductie. Afhankelijk van de disciplines van het team kan Mamedia ook animaties, grafische vormgeving en webdesign verzorgen.">
  <meta name="keywords" content="mamedia, mediacollege, amsterdam, stagiair, videoproductie, animaties, grafische vormgeving, webdesign">
</head>
<body>
  <?php include_once("includes/nav.php"); ?>
    <div class="videoParent" id="videoParent">
      <div id="videoContainer">
        <iframe class="youtube-vid-full" height="1080" width="1090"  src="https://www.youtube-nocookie.com/embed/<?php echo $pageContent["home_videoOverlay_videoURL"]; ?>?loop=1&controls=0&showinfo=0&autoplay=1&mute=1&playlist=<?php echo $pageContent["home_videoOverlay_videoURL"]; ?>"  frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
        <div id="cornerMedia">
            <div id="cornerMediaWrapper">

                <a href="<?php echo $pageContent["home_videoOverlay_socials_youtube_link"]; ?>" target="_blank"><i class="fab fa-youtube"></i></a>
                <a href="<?php echo $pageContent["home_videoOverlay_socials_facebook_link"]; ?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
                <a href="<?php echo $pageContent["home_videoOverlay_socials_instagram_link"]; ?>" target="_blank"><i class="fab fa-instagram"></i></a>

            </div>
        </div>
        <div class="blockVideoPanels"></div>
    </div>

    <div id="projectenPage">
      <h1>Projecten</h1>
        <div class="projectLibary_container">

            <div class="mid-container arrow-Holder">
                <div class="arrow arrow-left" id="arrow-left"></div>
                <div class="arrow" id="arrow-right"></div>

                <div class="one-two-portfolio">

                    <!--Portfolio items will be showed here-->

                </div>

                <div class="clear"></div>
            </div>

        </div>
    </div>




    <div id="amsterdammertjesSection">

      <div class="mid-container">

      <div id="amsterdammertjesLeft">
        <img src="<?php echo $pageContent["home_amsterdammertjes_image"]; ?>" alt="Mamedia - Amsterdammertjes">
      </div>
      <div id="amsterdammertjesRight">
        <h2><?php echo $pageContent["home_amsterdammertjes_title"]; ?></h2>
        <p><?php echo $pageContent["home_amsterdammertjes_description"]; ?></p>
        <div id="amsterdammertjesMediaHolder">

          <a href="http://www.salto.nl/programma/amsterdammertjes/" target="_blank">
              <button class="whiteButtonBottom"><?php echo $pageContent["home_amsterdammertjes_button_text"]; ?></button>
          </a>

          <a href="<?php echo $pageContent["home_amsterdammertjes_youtube_link"]; ?>" target="_blank" id="youtubeButton2"> <img src="images/youtubelogowhite.svg" alt="Amsterdammertjes YouTube logo"> </a>
          <a href="<?php echo $pageContent["home_amsterdammertjes_instagram_link"]; ?>" target="_blank" id="instagramButton2"> <img src="images/instagram.svg" alt="Amsterdammertjes Instagram icon"> </a>
        </div>
      </div>
    </div>
<div class="clear"></div>
    </div>

    <div id="teamSectie">
      <h2>Het team</h2>
      <div class="teamHolder">

          <?php
// Begin Maurice

//          This code loads and shows every colleague in to the site
          $getAllColleagues_sql = $db->prepare('SELECT * FROM `colleague`');
          $getAllColleagues_sql->execute();

          while($getAllColleagues_result = $getAllColleagues_sql->fetch( PDO::FETCH_OBJ )) {

              echo '<div class="teamGenootHolder">
              <img src="'. nl2br(htmlentities($getAllColleagues_result->image)) .'" alt="'. nl2br(htmlentities($getAllColleagues_result->name)) .'">
              <h3>'. nl2br(htmlentities($getAllColleagues_result->name)) .'</h3>
              <p>'. nl2br(htmlentities($getAllColleagues_result->description)) .'</p>
              </div>';

          }
//          Eind Maurice

          ?>

      </div>
    </div>

    <div id="samenwerkenSectie">
    <h2><?php echo $pageContent["home_callToAction_title"]; ?></h2>
    <div id="samenwerkenSectieButtonHolder">
    <a href="/stage"><button class="whiteButtonBottomFooter" type="button" name="button"><?php echo $pageContent["home_callToAction_stagiair"]; ?></button></a>
    <a href="/samenwerken"><button class="whiteButtonBottomFooter" type="button" name="button"><?php echo $pageContent["home_callToAction_samenwerken"]; ?></button></a>
    </div>
    </div>

  <?php include_once("includes/footer.php");

//  Begin Maurice
  $projectItemVideoURL = array();
  $projectTitle = array();
  $tumbnailURL = array();
  $gradientColorArray = array();

  $getAllPortfolioItems_sql = $db->prepare('SELECT * FROM portfolio_items ORDER BY RAND()');
  $getAllPortfolioItems_sql->execute();

  while($getAllPortfolioItems_result = $getAllPortfolioItems_sql->fetch( PDO::FETCH_OBJ )) {
      array_push($projectItemVideoURL, htmlspecialchars($getAllPortfolioItems_result->video_url, ENT_COMPAT,'ISO-8859-1', true));
      array_push($projectTitle, htmlspecialchars($getAllPortfolioItems_result->title, ENT_COMPAT,'ISO-8859-1', true));
      array_push($tumbnailURL, htmlspecialchars($getAllPortfolioItems_result->tumbnail_url, ENT_COMPAT,'ISO-8859-1', true));


      switch (rand (1, 3)) {
          case 1:
              $gradientColor = "gradient-black_blue";
              break;
          case 2:
              $gradientColor = "gradient-blue_purple";
              break;
          case 3:
              $gradientColor = "gradient-purple_black";
              break;
      }

      array_push($gradientColorArray, $gradientColor);
  }


  $project_items = array(
      "projectItemVideoURL" => $projectItemVideoURL,
      "projectTitle" => $projectTitle,
      "tumbnailURL" => $tumbnailURL,
      "gradient_color" => $gradientColorArray
  );

//  Eind Maurice
  ?>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script>
      var portfolioResults = JSON.parse('<?php echo json_encode($project_items); ?>');
  </script>
  <script src="js/portfolioItems.js"></script>
  <script src="js/navStickySwitch.js"></script>
  <script src="/js/hamburgerMenu.js"></script>
</body>
</html>
