<?php
//Code in dit bestand is gemaakt door Maurice de Jong - MD3A

$form_email_value = $_POST["email"];
$form_functie_value = $_POST["functie"];
$form_motivatie_value = $_POST["motivatie"];
$form_file_attachment = $_FILES["attachment"];


//Checks if everything is filled in
if (!empty($form_email_value) && !empty($form_functie_value) && !empty($form_motivatie_value) && !empty($form_file_attachment)) {

    $recaptcha_api_uri = 'https://www.google.com/recaptcha/api/siteverify';
    $_secret = '6Lc2_qIUAAAAAIH4iDh2uQmpMPaLUIBErdizQNf4';

    $data = array(
        'secret' => $_secret,
        'response' => $_POST['g-recaptcha-response'],
    );

    $verify = curl_init();
    curl_setopt($verify, CURLOPT_URL, $recaptcha_api_uri);
    curl_setopt($verify, CURLOPT_POST, true);
    curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);

    $rawResponse = curl_exec($verify);

    $apiResponse = json_decode($rawResponse, true);

    if (isset($apiResponse['success']) && true == $apiResponse['success']) {




$target_dir = "../images/formulier/";
$target_file = $target_dir . basename($_FILES["attachment"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

// Allow certain file formats
if($imageFileType != "pdf") {
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
//    echo "Sorry, your file was not uploaded.";
    header("Location: /stage.php?form=Pdf is niet geupload&email=".urlencode($form_email_value)."&functie=".urlencode($form_functie_value)."&motivatie=".urlencode($form_motivatie_value));
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["attachment"]["tmp_name"], $target_file)) {
//        echo "The file ". basename( $_FILES["attachment"]["name"]). " has been uploaded.";
    } else {
//        echo "Sorry, there was an error uploading your file.";
        header("Location: /stage.php?form=Kon de pdf niet versturenemail=".urlencode($form_email_value)."&functie=".urlencode($form_functie_value)."&motivatie=".urlencode($form_motivatie_value));
    }
}

        require_once('../includes/php/db_connection.php');

        $page_id = "stage";
        $getAllFormSettings_sql = $db->prepare('SELECT `sendMailTo`, `cc` FROM `form_settings` WHERE `page_id` = ?');
        $getAllFormSettings_sql->bindParam(1, $page_id);
        $getAllFormSettings_sql->execute();

        $getAllFormSettings_result = $getAllFormSettings_sql->fetch(PDO::FETCH_OBJ);

        if (!isset($getAllFormSettings_result->sendMailTo) && empty($getAllFormSettings_result->sendMailTo)) {
            die();
        }

//recipient
        $to = $getAllFormSettings_result->sendMailTo;

//sender
        $from = 'mamedia@ma-web.nl';
        $fromName = 'Mamedia Website';

//email subject
        $subject = 'MaMedia - Stage forumulier is ingevuld';

//attachment file path
        $file = $target_file;

//email body content
        $htmlContent = '
                    <html lang="nl">
                    <body>
                    <h2>Stage forumulier is ingevuld</h2>
                    <p><b>Email:</b> ' . nl2br(htmlspecialchars(strip_tags($form_email_value), ENT_COMPAT,'ISO-8859-1', true)) . '</p>
                    <p><b>Functie:</b> ' . nl2br(htmlspecialchars(strip_tags($form_functie_value), ENT_COMPAT,'ISO-8859-1', true)) . '</p>
                    <p><b>Bericht:</b><br/> ' . nl2br(htmlspecialchars(strip_tags($form_motivatie_value), ENT_COMPAT,'ISO-8859-1', true)) . '</p>
                    <p>Dit bericht is verzonden via de website MaMedia.nl</p>
                    </body>
                    </html>';

//header for sender info
        $headers = "From: $fromName"." <".$from.">";

//boundary
        $semi_rand = md5(time());
        $mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";

//headers for attachment
        $headers .= "\nMIME-Version: 1.0\n" . "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\"";
        $headers .= "\r\n" . "Reply-To: ".$form_email_value;

        if (isset($getAllFormSettings_result->cc) && !empty($getAllFormSettings_result->cc)) {
            $headers .= "\r\n" . 'Cc: '. $getAllFormSettings_result->cc;
        }
//multipart boundary
        $message = "--{$mime_boundary}\n" . "Content-Type: text/html; charset=\"UTF-8\"\n" .
            "Content-Transfer-Encoding: 7bit\n\n" . $htmlContent . "\n\n";

//preparing attachment
        if(!empty($file) > 0){
            if(is_file($file)){
                $message .= "--{$mime_boundary}\n";
                $fp =    @fopen($file,"rb");
                $data =  @fread($fp,filesize($file));

                @fclose($fp);
                $data = chunk_split(base64_encode($data));
                $message .= "Content-Type: application/octet-stream; name=\"".basename($file)."\"\n" .
                    "Content-Description: ".basename($file)."\n" .
                    "Content-Disposition: attachment;\n" . " filename=\"".basename($file)."\"; size=".filesize($file).";\n" .
                    "Content-Transfer-Encoding: base64\n\n" . $data . "\n\n";
            }
        }
        $message .= "--{$mime_boundary}--";
        $returnpath = "-f" . $from;

//send email
        $mail = @mail($to, $subject, $message, $headers, $returnpath);

//email sending status
        if ($mail){
            header("Location: /stage.php?form=Mail succesvol verzonden");
        } else {
            header("Location: /stage.php?form=Mail kon niet verzonden worden&email=".urlencode($form_email_value)."&functie=".urlencode($form_functie_value)."&motivatie=".urlencode($form_motivatie_value));
        }

        unlink($target_file);





    } else {
        header("Location: /stage.php?form=Ongeldige Recapcha&email=".urlencode($form_email_value)."&functie=".urlencode($form_functie_value)."&motivatie=".urlencode($form_motivatie_value));
    }

}

