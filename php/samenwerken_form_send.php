<?php
//Code in dit bestand is gemaakt door Maurice de Jong - MD3A

$form_email_value = $form_functie_value = $form_motivatie_value = "";
$form_email_value = $_POST["email"];
$form_functie_value = $_POST["functie"];
$form_motivatie_value = $_POST["motivatie"];

//Checks if everything is filled in
if (!empty($form_email_value) && !empty($form_functie_value) && !empty($form_motivatie_value)) {

    $recaptcha_api_uri = 'https://www.google.com/recaptcha/api/siteverify';
    $_secret = '6Lc2_qIUAAAAAIH4iDh2uQmpMPaLUIBErdizQNf4';
    $data = array(
        'secret' => $_secret,
        'response' => $_POST['g-recaptcha-response'],
    );

    $verify = curl_init();
    curl_setopt($verify, CURLOPT_URL, $recaptcha_api_uri);
    curl_setopt($verify, CURLOPT_POST, true);
    curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
    $rawResponse = curl_exec($verify);

    $apiResponse = json_decode($rawResponse, true);


    if (isset($apiResponse['success']) && true == $apiResponse['success']) {


        $postData = $uploadedFile = $statusMsg = '';
            // Get the submitted form data
            $postData = $_POST;


                // Validate email
                if (filter_var($form_email_value, FILTER_VALIDATE_EMAIL) === false) {
//                    $statusMsg = 'Please enter your valid email.';
//                    $form_responce = "mail_not_valid";
                    header("Location: /samenwerken.php?form=Ongeldig email&email=".urlencode($form_email_value)."&functie=".urlencode($form_functie_value)."&motivatie=".urlencode($form_motivatie_value));
                } else {
                    $uploadStatus = 1;


                    if ($uploadStatus == 1) {


                        require_once('../includes/php/db_connection.php');

                        $page_id = "samenwerken";
                        $getAllFormSettings_sql = $db->prepare('SELECT `sendMailTo`, `cc` FROM `form_settings` WHERE `page_id` = ?');
                        $getAllFormSettings_sql->bindParam(1, $page_id);
                        $getAllFormSettings_sql->execute();

                        $getAllFormSettings_result = $getAllFormSettings_sql->fetch(PDO::FETCH_OBJ);

                        if (!isset($getAllFormSettings_result->sendMailTo) && empty($getAllFormSettings_result->sendMailTo)) {
                            die();
                        }

                        // Recipient
                        $toEmail = $getAllFormSettings_result->sendMailTo;

                        // Sender
                        $from = 'mamedia@ma-web.nl';
                        $fromName = 'Mamedia Website';

                        // Subject
                        $emailSubject = 'MaMedia - Samenwerkings forumulier is ingevuld';

                        // Message
                        $htmlContent = '
                    <html lang="nl">
                    <body>
                    <h2>Samenwerkings forumulier is ingevuld</h2>
                    <p><b>Email:</b> ' . nl2br(htmlspecialchars(strip_tags($form_email_value), ENT_COMPAT,'ISO-8859-1', true)) . '</p>
                    <p><b>Functie:</b> ' . nl2br(htmlspecialchars(strip_tags($form_functie_value), ENT_COMPAT,'ISO-8859-1', true)) . '</p>
                    <p><b>Bericht:</b><br/> ' . nl2br(htmlspecialchars(strip_tags($form_motivatie_value), ENT_COMPAT,'ISO-8859-1', true)) . '</p>
                    <p>Dit bericht is verzonden via de website MaMedia.nl</p>
                    </body>
                    </html>';

                        // Header for sender info
                        $headers = "From: $fromName" . " <" . $from . ">";

                        // Set content-type header for sending HTML email
                        $headers .= "\r\n" . "MIME-Version: 1.0";
                        $headers .= "\r\n" . "Content-type:text/html;charset=UTF-8";
                        $headers .= "\r\n" . "Reply-To: ".$form_email_value;

                        if (isset($getAllFormSettings_result->cc) && !empty($getAllFormSettings_result->cc)) {
                            $headers .= "\r\n" . 'Cc: '. $getAllFormSettings_result->cc;
                        }

                        // Send email
                        $mail = mail($toEmail, $emailSubject, $htmlContent, $headers);

                        // Checks if the mail is sent
                        if ($mail) {
//                            $form_responce = "mail_send_succesfully";
                            header("Location: /samenwerken.php?form=Mail succesvol verzonden");

                        } else {
//                            $form_responce = "mail_send_fail";
                            header("Location: /samenwerken.php?form=Mail kon niet verzonden worden&email=".urlencode($form_email_value)."&functie=".urlencode($form_functie_value)."&motivatie=".urlencode($form_motivatie_value));

                        }
                    }
                }


    } else {
//        $form_responce = "recapcha_error";
        header("Location: /samenwerken.php?form=Recapcha niet ingevuld&email=".urlencode($form_email_value)."&functie=".urlencode($form_functie_value)."&motivatie=".urlencode($form_motivatie_value));
    }



}
