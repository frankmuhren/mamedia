<?php
class pageContentFooter {}
$page_id = "footer";
 $getAllPageContent_sql = $db->prepare('SELECT `title`, `value` FROM `page_content` WHERE `page_id` = ?');
 $getAllPageContent_sql->bindParam(1, $page_id);
 $getAllPageContent_sql->execute();

 while($getAllPageContent_result = $getAllPageContent_sql->fetch( PDO::FETCH_OBJ )) {
     $pageContentFooter[$getAllPageContent_result->title] = nl2br(htmlspecialchars($getAllPageContent_result->value, ENT_COMPAT,'ISO-8859-1', true));
 }

?>
<footer>
    <div class="mid-container">
        <div class="footerdiv">
            <a href="/"> <img src="/images/mamedialogo.png" class="footer-logo" alt="MaMedia logo"> </a>
            <p class="margin-top-0"><?php echo $pageContentFooter["footer_street"]; ?></p>
            <p><?php echo $pageContentFooter["footer_postalcode"]; ?></p>

        </div>
        <div class="footerdiv">
            <h2>Postadres</h2>
            <p><?php echo $pageContentFooter["footer_name"]; ?></p>
            <p><?php echo $pageContentFooter["footer_tav"]; ?></p>
            <p><?php echo $pageContentFooter["footer_mailbox"]; ?></p>
            <p><?php echo $pageContentFooter["footer_postalcode2"]; ?></p>
        </div>
        <div class="footerdiv">
            <h2>Telefoon</h2>
            <a href="tel://<?php echo $pageContentFooter["footer_phonenumber"]; ?>"><?php echo $pageContentFooter["footer_phonenumber"]; ?></a>
            <h2>E-mail</h2>
            <a href="<?php echo $pageContentFooter["footer_email"]; ?>"><?php echo $pageContentFooter["footer_email"]; ?></a>
        </div>
        <div class="footerdiv">
            <h2>Social Media</h2>
            <div id="footerMediaHolder">
                <ul>
                    <a href="<?php echo $pageContentFooter["footer_youtube_link"]; ?>"><li class="fab fa-youtube"></li></a>
                    <a href="<?php echo $pageContentFooter["footer_facebook_link"]; ?>"><li class="fab fa-facebook-f"></li></a>
                    <a href="<?php echo $pageContentFooter["footer_instagram_link"]; ?>"><li class="fab fa-instagram"></li></a>
                </ul>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
</footer>
