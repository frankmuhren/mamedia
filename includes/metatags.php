        <meta charset="utf-8">
        <meta name="robots" content="index, follow">
        <meta name="revisit-after" content="3 days">
        <meta name="language" content="nl">
        <meta name="web_author" content="Maurice de Jong & Frank Mühren & Melissa Vriend">
        <meta name="mobile-web-app-capable" content="yes">

        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <link rel="manifest" href="/manifest.json">
        <link rel="canonical" href="https://mamedia.nl/">

        <link rel="icon" href="/images/favicon.ico" type="image/*" sizes="16x16">
        <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">

        <link rel="icon" sizes="192x192" href="/images/favicon.png">
        <meta name="theme-color" content="#E62686">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta property="og:url" content="https://mamedia.nl/">
        <meta property="og:title" content="MaMedia">
        <meta property="og:image" content="/images/favicon.png">
        <meta property="og:site_name" content="mamedia.nl">
        <meta property="og:type" content="website">
        <meta property="og:description" content="-">

        <meta name="twitter:image" content="/images/favicon.png">
        <meta name="twitter:title" content="MaMedia">
        <meta name="twitter:description" content="-">
        <meta name="twitter:site" content="https://mamedia.nl/">

      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<!--        <link rel="stylesheet" type="text/css" href="css/style.css">-->

        <link rel="stylesheet" type="text/css" href="/css/algemeen.css">
        <link rel="stylesheet" type="text/css" href="/css/nav.css">
        <link rel="stylesheet" type="text/css" href="/css/portfolioItems.css">
        <link rel="stylesheet" type="text/css" href="/css/landingspage.css">
        <link rel="stylesheet" type="text/css" href="/css/footer.css">
        <link rel="stylesheet" type="text/css" href="/css/stage.css">

        <link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png">
        <link rel="mask-icon" href="/images/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#9f00a7">