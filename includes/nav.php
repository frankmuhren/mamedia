<!-- Navigatie computer / mobile gemaakt door:
 Maurice de Jong
 klas: MD3A -->
<?php

if ($_SERVER['REQUEST_URI'] == "/index.php" || $_SERVER['REQUEST_URI'] == "/index" || $_SERVER['REQUEST_URI'] == "/" || $_SERVER['REQUEST_URI'] == "") {
    $navState = "transparent";
} else {
    $navState = "sticky";
    echo '<div class="nav-sticky-fix"></div>';
}

function navCorrectLink($link) {
    if ($link != "/index") {
        if ($_SERVER['REQUEST_URI'] == $link || $_SERVER['REQUEST_URI'] == $link . ".php") {
            return 'class="selected"';
        }
    } else {
        if ($_SERVER['REQUEST_URI'] == "/index.php" || $_SERVER['REQUEST_URI'] == "/index" || $_SERVER['REQUEST_URI'] == "/" || $_SERVER['REQUEST_URI'] == "") {
            return 'class="selected"';
        }
    }
}


?>
<div class="mobile-nav"><a href="/"> <img src="/images/mamedialogo.png" class="nav-mobile-logo" alt="MaMedia logo"> </a>
    <div class="mobile-ham-block" id="mobile-ham-block">
        <div></div>
    </div>
</div>
<nav class="<?php echo $navState; ?>">

    <div class="mid-container position-relative">
        <a href="/"> <img src="/images/mamedialogo.png" class="nav-logo" alt="MaMedia logo"> </a>


        <ul>
            <a href="/"><li <?php echo navCorrectLink("/index"); ?>>Home</li></a>
            <a href="/stage"><li <?php echo navCorrectLink("/stage"); ?>>Stage</li></a>
            <a href="/samenwerken"><li <?php echo navCorrectLink("/samenwerken"); ?>>Samenwerken</li></a>
            <a href="http://www.salto.nl/programma/amsterdammertjes/" target="_blank"><li>Amsterdammertjes</li></a>
        </ul>
    </div>

</nav>