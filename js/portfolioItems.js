/**
 * Code in dit bestand is gemaakt door Maurice de Jong - MD3A
 * Date: 11-4-2019
 **/
$(document).ready(function () {

    $totalDisplayedItems = 1;
    $totalItems = portfolioResults.projectItemVideoURL.length;
    $forStartNumber = 0;
    $forLoopTimes = 0;

    $windowState = 0;
    $returnPortfolioItem = function($projectItemVideoURL, $projectTitle, $tumbnailURL, $leeg, $gradientColor) {
        if ($leeg) {
            return '<div class="portfolioItem-Animation"></div>';
        } else {
            // var decoded = $("").html($projectTitle).text();
            var decoded = decodeURIComponent(($projectTitle+'').replace(/\+/g, '%20'));

            return '<span itemscope itemtype="http://schema.org/Movie">' +
                '<div class="portfolioItem-Animation">'+
                '<a href="/portfolio/'+ $projectItemVideoURL +'" itemprop="url">'+
                '<div class="portfolioItem">'+
                '<img src="'+ $tumbnailURL +'" alt="'+ decoded +'" itemprop="image">'+
                '<div class="portfolioItem-gradient '+$gradientColor+'">'+
                '<div class="portfolioItem-title" itemprop="name">'+decoded+'</div>'+
                '<div class="portfolioItem-button">Bekijk project</div>'+
                '</div>'+
                '</div></a></div>' +
                '</span>';
        }
    };




    $updateItems = function($startNumber) {

        $('.one-two-portfolio').empty();
        for ($i = 0; $i < $totalDisplayedItems; $i++) {

            $arrayID = $startNumber + $i;

            if (portfolioResults.projectItemVideoURL[$arrayID] !== undefined) {
                $('.one-two-portfolio').append($returnPortfolioItem(portfolioResults.projectItemVideoURL[$arrayID], portfolioResults.projectTitle[$arrayID], portfolioResults.tumbnailURL[$arrayID], false, portfolioResults.gradient_color[$arrayID]));

            } else {
                $('.one-two-portfolio').append($returnPortfolioItem(null, "", "", true));

            }
        }

    };


    $fixPortfolioItems = function () {
        $windowWidth = $(window).width();

        if ($windowWidth > 891 && $windowState !== 1) {
            $windowState = 1;
            $totalDisplayedItems = 3;
            $updateItems($forStartNumber);
        } else if ($windowWidth < 891 && $windowWidth > 723 && $windowState !== 2) {
            $windowState = 2;
            $totalDisplayedItems = 2;
            $updateItems($forStartNumber);
        } else if ($windowWidth < 600 && $windowState !== 3) {
            $windowState = 3;
            $totalDisplayedItems = 1;
            $updateItems($forStartNumber);
        }
    };


    $nextPortfolioItems = function() {
        $fixPortfolioItems();

        $forStartNumber = $forStartNumber + $totalDisplayedItems;
        if (($forStartNumber + $totalDisplayedItems) === $totalItems || ($forStartNumber + $totalDisplayedItems) >= $totalItems) {

            $forStartNumber = $totalItems - $totalDisplayedItems;
        }
        $('.one-two-portfolio').fadeOut(function () {
            $updateItems($forStartNumber);
        }).fadeIn();

    };

    $previousPortfolioItems = function() {
        $fixPortfolioItems();

        $forStartNumber = $forStartNumber - $totalDisplayedItems;
        if ($forStartNumber === 0 || $forStartNumber <= 0) {
            $forStartNumber = 0;
        }

        $('.one-two-portfolio').fadeOut(function () {
            $updateItems($forStartNumber);
        }).fadeIn();

    };


    $("#arrow-right").click(function () {
        $nextPortfolioItems();
    });

    $("#arrow-left").click(function () {
        $previousPortfolioItems();
    });

    $(window).resize(function () {
        $fixPortfolioItems();
    });
    $fixPortfolioItems();

});