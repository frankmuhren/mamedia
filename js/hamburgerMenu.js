/**
 * Code in dit bestand is gemaakt door Maurice de Jong - MD3A
 * Date: 7-5-2019
 **/
$(document).ready(function () {

    $mobileNavState = 1;

    $("#mobile-ham-block").click(function () {
        if ($mobileNavState) {
            $("nav").addClass("open");

            $mobileNavState = 0;
        } else {
            $("nav").removeClass("open");
            $mobileNavState = 1;
        }
    });

});