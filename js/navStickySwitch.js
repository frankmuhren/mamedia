/**
 * Code in dit bestand is gemaakt door Maurice de Jong - MD3A
 * Date: 23-4-2019
 **/
$(document).ready(function () {

    $navFix = function() {
        //If screen size is lower then 600px give the nav the class sticky
     if ($(window).scrollTop() >= ($("#videoParent").offset().top + $("#videoParent").outerHeight())) {
            //Gives nav the class sticky
            $("nav.transparent").addClass("sticky");
            $("nav.transparent").removeClass("transparent");
        } else {
            //Gives nav the class transparent
            $("nav.sticky").addClass("transparent");
            $("nav.sticky").removeClass("sticky");
        }
    };

    //Event listeners
    $(document).scroll(function() { $navFix(); });
    $(window).resize(function () { $navFix(); });

    //Runs the function when the page is loaded
    $navFix();
});