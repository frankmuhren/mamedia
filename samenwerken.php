<?php
/**
 * Front-end in dit bestand is gemaakt door Melissa Vriend - MD3A
 * PHP in dit bestand is gemaakt door Maurice de Jong - MD3A
 */

//  Begin Maurice
require_once('includes/php/db_connection.php');


$page_id = "samenwerken";
$getAllPageContent_sql = $db->prepare('SELECT `title`, `value` FROM `page_content` WHERE `page_id` = ?');
$getAllPageContent_sql->bindParam(1, $page_id);
$getAllPageContent_sql->execute();

while ($getAllPageContent_result = $getAllPageContent_sql->fetch(PDO::FETCH_OBJ)) {
    $pageContent[$getAllPageContent_result->title] = nl2br(htmlspecialchars(strip_tags($getAllPageContent_result->value), ENT_COMPAT,'ISO-8859-1', true));
}

//  Eind Maurice

?>

<!DOCTYPE html>
<html lang="nl" dir="ltr">

<head>
    <?php include_once("includes/metatags.php"); ?>
    <link rel="canonical" href="https://<?php echo htmlentities($_SERVER['SERVER_NAME']); ?>/samenwerken" />
    <meta name="application-name" content="MaMedia">
    <title>MaMedia - Zullen we samenwerken?</title>
    <meta name="description" content="De diensten van Mamedia bestaan hoofdzakelijk uit videoproductie. Afhankelijk van de disciplines van het team kan Mamedia ook animaties, grafische vormgeving en webdesign verzorgen.">
    <meta name="keywords" content="mamedia, mediacollege, amsterdam, stagiair, videoproductie, animaties, grafische vormgeving, webdesign">
    <script src='https://www.google.com/recaptcha/api.js' async defer></script>
</head>

<body>
    <?php include_once("includes/nav.php"); ?>

    <!-- begin Gradient  -->
    <div class="wrapper_gradient">
        <div class="mid-container">
            <div class="video_text">
                <!-- filmpje class \/ -->
                <div class="stage_video">
                    <iframe src="https://www.youtube-nocookie.com/embed/<?php echo $pageContent["samenwerken_videoURL"]; ?>?loop=1&controls=0&showinfo=0&autoplay=1&mute=1&playlist=<?php echo $pageContent["samenwerken_videoURL"]; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>

            <div class="video_text">
                <div class="stage_text">
                    <h1><?php echo $pageContent["samenwerken_pageTitle"]; ?></h1>
                    <p>
                        <?php echo $pageContent["samenwerken_pageDescription"]; ?>
                    </p>
                </div>
            </div>
            <div class="clear"></div>
        </div>


        <!-- Begin Quote fade in fade out -->

<!--        <div class="big_quote">-->
<!--            <h1><span><svg width="23.999999999999996" height="23.999999999999996" xmlns="http://www.w3.org/2000/svg">-->
<!---->
<!--                        <g>-->
<!--                            <title>background</title>-->
<!--                            <rect fill="none" id="canvas_background" height="402" width="582" y="-1" x="-1" />-->
<!--                        </g>-->
<!--                        <g>-->
<!--                            <title>Layer 1</title>-->
<!--                            <path fill="#ffffff" id="svg_1" d="m13.944902,21.036049l0,-7.391c0,-5.704 3.731,-9.57 8.983,-10.609l0.995,2.151c-2.432,0.917 -3.995,3.638 -3.995,5.849l4,0l0,10l-9.983,0zm-14.017,0l0,-7.391c0,-5.704 3.748,-9.57 9,-10.609l0.996,2.151c-2.433,0.917 -3.996,3.638 -3.996,5.849l3.983,0l0,10l-9.983,0z" />-->
<!--                        </g>-->
<!--                    </svg></span>-->
<!---->
<!--                <div id="testimonial-container">-->
<!--                    <span>Lorem nogwat ipsum</span>-->
<!--                    <span>ipsum nogwat lorum</span>-->
<!--                </div>-->
<!---->
<!--                <span><svg width="23.999999999999996" height="23.999999999999996" xmlns="http://www.w3.org/2000/svg">-->
<!---->
<!--                        <g>-->
<!--                            <title>background</title>-->
<!--                            <rect fill="none" id="canvas_background" height="402" width="582" y="-1" x="-1" />-->
<!--                        </g>-->
<!--                        <g>-->
<!--                            <title>Layer 1</title>-->
<!--                            <path transform="rotate(-180 11.927902221679688,12.036048889160156) " fill="#ffffff" id="svg_1" d="m13.944902,21.036049l0,-7.391c0,-5.704 3.731,-9.57 8.983,-10.609l0.995,2.151c-2.432,0.917 -3.995,3.638 -3.995,5.849l4,0l0,10l-9.983,0zm-14.017,0l0,-7.391c0,-5.704 3.748,-9.57 9,-10.609l0.996,2.151c-2.433,0.917 -3.996,3.638 -3.996,5.849l3.983,0l0,10l-9.983,0z" />-->
<!--                        </g>-->
<!--                    </svg></span>-->
<!--            </h1>-->
<!--        </div>-->



        <div class="big_quote">
            <span><svg width="23.999999999999996" height="23.999999999999996" xmlns="http://www.w3.org/2000/svg">

                        <g>
                            <title>background</title>
                            <rect fill="none" id="canvas_background" height="402" width="582" y="-1" x="-1" />
                        </g>
                        <g>
                            <title>Layer 1</title>
                            <path fill="#ffffff" id="svg_1" d="m13.944902,21.036049l0,-7.391c0,-5.704 3.731,-9.57 8.983,-10.609l0.995,2.151c-2.432,0.917 -3.995,3.638 -3.995,5.849l4,0l0,10l-9.983,0zm-14.017,0l0,-7.391c0,-5.704 3.748,-9.57 9,-10.609l0.996,2.151c-2.433,0.917 -3.996,3.638 -3.996,5.849l3.983,0l0,10l-9.983,0z" />
                        </g>
                    </svg>

                <span id="testimonialValue"></span>

                <svg width="23.999999999999996" height="23.999999999999996" xmlns="http://www.w3.org/2000/svg">

                        <g>
                            <title>background</title>
                            <rect fill="none" id="canvas_background" height="402" width="582" y="-1" x="-1" />
                        </g>
                        <g>
                            <title>Layer 1</title>
                            <path transform="rotate(-180 11.927902221679688,12.036048889160156) " fill="#ffffff" id="svg_1" d="m13.944902,21.036049l0,-7.391c0,-5.704 3.731,-9.57 8.983,-10.609l0.995,2.151c-2.432,0.917 -3.995,3.638 -3.995,5.849l4,0l0,10l-9.983,0zm-14.017,0l0,-7.391c0,-5.704 3.748,-9.57 9,-10.609l0.996,2.151c-2.433,0.917 -3.996,3.638 -3.996,5.849l3.983,0l0,10l-9.983,0z" />
                        </g>
                    </svg>
            </span>
        </div>




        <!-- end quote fade in fade out -->

    </div>
    <!-- end gradient  -->




    <!-- Begin Formulier stage -->

    <div class="wrapper_form">
        <div class="mid-container">
            <div class="two_blocks">
                <div class="form_leftInfo">
                    <h1><?php echo $pageContent["samenwerken_left_form_heading"]; ?></h1>
                    <p>
                        <?php echo $pageContent["samenwerken_left_form_description"]; ?>
                    </p>
                </div>
            </div>
            <div class="two_blocks">
                <div class="form_rightInfo">
                    <h1 id="samenwerkenFormTitle"><?php echo $pageContent["samenwerken_form_title"]; ?></h1>
                    <div class="form_input">

                        <?php

                        if (isset($_GET["form"]) && !empty($_GET["form"]) && urldecode($_GET["form"]) == "Mail succesvol verzonden") {

                            echo '<div style="text-align: center;font-weight: 600;font-size: 21px;">'.htmlentities($_GET["form"]).'</div>';

                        } else {
                            $email = $functie = $motivatie = $form = "";
                            if (isset($_GET["email"])){
                                $email = $_GET["email"];
                            }
                            if (isset($_GET["functie"])){
                                $functie = $_GET["functie"];
                            }
                            if (isset($_GET["motivatie"])){
                                $motivatie = $_GET["motivatie"];
                            }
                            if (isset($_GET["form"])){
                                $form = $_GET["form"];
                            }
                        ?>
                            <div style="text-align: center;font-weight: 600;font-size: 21px;margin-bottom: 15px;"><?php echo htmlentities($form); ?></div>
                        <form id="samenwerkenFormulier" action="/php/samenwerken_form_send.php" method="post">
                            <input type="text" name="email" placeholder="E-mail" id="form_email" value="<?php echo htmlentities(urldecode($email)); ?>" required>
                            <input type="text" name="functie" placeholder="Functie" id="form_functie" value="<?php echo htmlentities(urldecode($functie)); ?>" required>
                            <br>
                            <textarea type="text" rows="10" name="motivatie" placeholder="Motivatie" id="form_motivatie" required><?php echo htmlentities(urldecode($motivatie)); ?></textarea><br>
                            <div class="g-recaptcha" data-sitekey="6Lc2_qIUAAAAAOliNxPzH3x6XjRaPQUg2O0cvhkx"></div>
                            <br>
                            <input type="submit" class="button1 send cursor-pointer" id="samenwerken_submit" value="Verstuur">
                        </form>
                        <?php } ?>
                    </div>
                </div>

            </div>
        </div>

    </div>


    <!-- end formulier page -->

    <?php include_once("includes/footer.php"); ?>


    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script>

        <?php

        //  Begin Maurice
        $testimonials = array();
        $pagina = "samenwerken";

        $getAllTestimonialItems_sql = $db->prepare('SELECT `testimonial_value` FROM testimonials WHERE `pagina` = ?');
        $getAllTestimonialItems_sql->bindParam(1, $pagina);
        $getAllTestimonialItems_sql->execute();

        while($getAllTestimonialItems_result = $getAllTestimonialItems_sql->fetch( PDO::FETCH_OBJ )) {
            array_push($testimonials, htmlentities($getAllTestimonialItems_result->testimonial_value));
        }

        $testimonial_items = array(
            "testimonials" => $testimonials,
        );
        //  Eind Maurice
        ?>
        var testimonialResults = JSON.parse('<?php echo json_encode($testimonial_items); ?>');

    </script>
    <script src="js/quotefade.js"></script>
    <script src="/js/hamburgerMenu.js"></script>
    <script src="/js/sendForm.js"></script>
</body>

</html>
