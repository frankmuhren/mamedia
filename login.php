<?php
//Code in dit bestand is gemaakt door Frank Muhren - MD3A
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(1);

session_start();

//date_default_timezone_set("Europe/Amsterdam");
require_once 'includes/php/db_connection.php';

$blocked = false;

$db_ip = hash('sha256', $_SERVER['REMOTE_ADDR']);
$select_ip = $db->prepare("SELECT * FROM `wrong_logins` WHERE `ip`= ?");
$select_ip->bindParam(1, $db_ip);
$select_ip->execute();
$select_ip_counter = $select_ip->rowCount();
$select_ip_result = $select_ip->fetch();


//echo "<script> window.alert('a: " . $timestamp . "'); </script>";

if (isset($select_ip_result["timestamp"]) && !empty($select_ip_result["timestamp"]) && $select_ip_counter == 1) {

    $date = new DateTime(null, new DateTimeZone('Europe/Amsterdam'));
    $timestamp = $date->getTimestamp();

//    echo "<Br>test7654567654<br>";


//    echo "<Br>Timestamp is <br>". $timestamp . '<br>';
    if ($timestamp >= $select_ip_result["timestamp"]) {

//        echo "test3";
        $delete_wrong_login = $db->prepare("DELETE FROM `wrong_logins` WHERE `ip`=?");
        $delete_wrong_login->bindParam(1, $db_ip);
        $delete_wrong_login->execute();
    } else {
        $blocked = true;
    }

}



if ($blocked == false && isset($_POST["submit"]) && $_POST["loginemail"] != "" && $_POST["password"] != "") {



$select = $db->prepare("SELECT * FROM `users` WHERE `email`=?");
$select->bindParam(1, $_POST["loginemail"]);
$select->execute();
$user_counter = $select->rowCount();
$db_user_result = $select->fetch();

    $salt = "&gv#@@Dv6rg^%Fg^g5EF";
    $pepper = "3FxTDg%fFESDFtferfszer";
    $form_input_password = hash('sha256', $salt.hash('sha256',$_POST["password"]).$pepper);

if ($_POST["loginemail"] == $db_user_result["email"] && $form_input_password == $db_user_result["password"]) {

    $n=10;
    function randomString($n) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($i = 0; $i < $n; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }

        return $randomString;
    }

    $generated_token = randomString($n);
$user_ip_enc = hash('sha256', $_SERVER["REMOTE_ADDR"]);

$update_user_token = $db->prepare("UPDATE `users` SET `token`=?,`user_ip`=? WHERE `email`=?");
$update_user_token->bindParam(1, $generated_token);
$update_user_token->bindParam(2, $user_ip_enc);
$update_user_token->bindParam(3, $_POST["loginemail"]);
$update_user_token->execute();

    $delete_wrong_login = $db->prepare("DELETE FROM `wrong_logins` WHERE `ip`=?");
    $delete_wrong_login->bindParam(1, $db_ip);
    $delete_wrong_login->execute();

$_SESSION["token"] = $generated_token;


header("Location: /admin/dashboard.php");
die();

} else {


    if ($select_ip_counter == 0 || $select_ip_counter == "0") {
//echo "test1";
        $insert_ip = $db->prepare("INSERT INTO `wrong_logins`(`ip`) VALUES (?)");
        $insert_ip->bindParam(1, $db_ip);
        $insert_ip->execute();


    } else if ($select_ip_result["attempts"] <= 4) {

//        echo "test4";
        echo $newAttempt = $select_ip_result["attempts"] + 1;

        $update_worng_login = $db->prepare("UPDATE `wrong_logins` SET `attempts`=? WHERE `ip` = ?");
        $update_worng_login ->bindParam(1, $newAttempt);
        $update_worng_login ->bindParam(2, $db_ip);
        $update_worng_login ->execute();

    } else if ($select_ip_result["attempts"] == 5 || $select_ip_result["attempts"] > 5) {

//        echo "test5";
        $date = new DateTime(null, new DateTimeZone('Europe/Amsterdam'));
        $timestamp = $date->getTimestamp();
        $newtimestamp = $timestamp + 300;

        $update_worng_login = $db->prepare("UPDATE `wrong_logins` SET `timestamp`=? WHERE `ip`=?");
        $update_worng_login ->bindParam(1, $newtimestamp);
        $update_worng_login ->bindParam(2, $db_ip);
        $update_worng_login ->execute();

    }

    header('Location: '. $_SERVER['REQUEST_URI'] . '?wrongLogin=1');
}

}




 ?>

<!DOCTYPE html>
<html lang="nl" dir="ltr">
  <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <link rel="canonical" href="https://<?php echo htmlentities($_SERVER['SERVER_NAME']); ?>/login" />
    <title>Mamedia - Admin login</title>
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
      <link rel="stylesheet" type="text/css" href="/css/algemeen.css">
      <link rel="stylesheet" type="text/css" href="/css/nav.css">
      <link rel="stylesheet" type="text/css" href="/css/portfolioItems.css">
      <link rel="stylesheet" type="text/css" href="/css/landingspage.css">
      <link rel="stylesheet" type="text/css" href="/css/footer.css">
      <link rel="stylesheet" type="text/css" href="/css/stage.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/footer.css">
    <link rel="stylesheet" href="/css/login.css">
  </head>
  <body style="background-color: #f4f4f4;">
<?php include_once("includes/nav.php"); ?>

<div id="loginContainer" style="min-height: calc(100vh - 455px);">
  <h1>Admin Login</h1>
  <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
  <input type="email" name="loginemail" placeholder="Email"> <br>
  <input type="password" name="password" placeholder="Wachtwoord" required><br>
  <input class="loginButton" type="submit" name="submit" value="Login">
</form>
<?php
if ($blocked == true) {
    echo '<p id="noAccesText">Uw account is 5 minuten geblokkeerd.</p>';
}else if (isset($_GET["wrongLogin"]) && $_GET["wrongLogin"] == 1) {
echo '<p id="noAccesText">De login gegevens zijn incorrect.</p>';
}

?>

</div>


  <?php include_once("includes/footer.php"); ?>

  </body>
</html>
